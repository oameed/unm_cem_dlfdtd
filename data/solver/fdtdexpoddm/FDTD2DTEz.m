%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                            %%%
%%% COMPUTATIONAL EM LAB                                                %%%
%%% DEEP LEARNING PROJECT                                               %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD                        %%%
%%% 2D FDTD / PRECONDITIONING MATRIX ('B') / APPROXIMATION MATRIX ('E') %%%
%%% by: OAMEED NOAKOASTEEN, SHU WANG                                    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% PHASE: 'fdtd'  : FDTD simulation
% PHASE: 'ddmB'  : DDM FDTD B MATRIX
% PHASE: 'ddmE'  : DDM FDTD E MATRIX
% LGBN :         : LIST OF GLOBAL NUMBERS

function [vector_out,list_glb_num,MPEX]=FDTD2DTEz(SPEC,TIMESTEP,PHASE,INITIAL,LGBN,SUBDOMDIV)

addpath(fullfile('..','..','lib','fdtdexpoddm'       ))
addpath(fullfile('..','..','lib','fdtdexpoddm','fdtd'))
addpath(fullfile('..','..','lib','fdtdexpoddm','ddm' ))

eps0 =8.85419*10^-12                                                                       ; % FREE SPACE    : PERMITTIVITY
mu0  =4*pi*10^-7                                                                           ; % FREE SPACE    : PERMEABILITY
nc   =40                                                                                   ; % MESH          : NUMBER OF CELLS PER WAVELENGTH
CS   =1/sqrt(2)                                                                            ; % MESH          : COURANT STABILITY FACTOR
A    =1                                                                                    ; % PULSE         : AMPLITUDE OF THE PULSE
fmax =2*10^9                                                                               ; % PULSE         : MAXIMUM FREQUENCY
Te   =6*10^-9                                                                              ; % PULSE         : END OF THE TIME DOMAIN PULSE

pulse=@(A,T,T0,TAU) A.*exp(-((T-T0)./TAU).^2)                                              ; % PULSE         : PULSE SHAPE
L    =SPEC(3)                                                                              ; % DOMAIN DESIGN : GEOMETRIC LENGTH OF DOMAIN (m)
PMLF =0.25                                                                                 ; % DOMAIN DESIGN : ALLOCATION OF PML
Npml =2                                                                                    ; % PML           : ORDER OF THE POWER-INCREASING SIGMA FUNCTION
R0   =10^-8                                                                                ; % PML           : REQUIRED LEVEL OF REFLECTION FROM PML
SIGMA=10^8                                                                                 ; % MAT PROP      : CONDUCTIVITY


if strcmp(PHASE,'fdtd')
    Nt    =TIMESTEP+1                                                                      ; % FDTD LOOP     : NUMBER OF TIME-STEPS
else
    if strcmp(PHASE,'ddmB') || strcmp(PHASE,'ddmE')
        Nt=1                                                                               ;
    end
end

%%% PRE-PROCESSINGS %%%
c0      =1/sqrt(mu0*eps0)                                                                  ; % FREE SPACE    : SPEED OF LIGHT (m/s)
LAMmin  =c0/fmax                                                                           ; % MESH          : SMALLEST LAMBDA
delta   =LAMmin/nc                                                                         ; % MESH          : DX=DY=DZ=DELTA
dt      =delta*CS/c0                                                                       ; % MESH/PULSE    : COURANT STABILITY CONDITION
PSFx    =floor((SPEC(1)*L)/delta)                                                          ; % DOMAIN DESIGN : X LOCATION OF POINT SOURCE
PSFy    =floor((SPEC(2)*L)/delta)                                                          ; % DOMAIN DESIGN : Y LOCATION OF POINT SOURCE
NPMLC   =floor((PMLF*L)/delta)                                                             ; % PML           : NUMBER OF PML CELLS
SIGMAMAX=-((Npml+1)*eps0*c0*log(R0))/(2*delta*NPMLC)                                       ; % PML           : POWER-INCREASING FUNCTION COEFFICIENT
%%% INITIALIZE PULSE %%%
if strcmp(PHASE,'fdtd')        
    tau =sqrt(-log(0.1))/(pi*fmax)                                                         ;
    t0  =sqrt(20)*tau                                                                      ;
    t   =0:dt:Te                                                                           ;
    P   =pulse(A,t,t0,tau)                                                                 ;
end

%%% INITIALZE FIELD ARRAYS %%%
if strcmp(PHASE,'fdtd')
    l  =0:delta:L                                                                          ;
    Ndx=size(l,2)-1                                                                        ;
    Ndy=size(l,2)-1                                                                        ;
    Ex =zeros(Ndy  ,Ndx       )                                                            ;
    Ey =zeros(Ndy  ,Ndx       )                                                            ;
    Hz =zeros(Ndy  ,Ndx  ,3   )                                                            ;
    EX =zeros(Ndy  ,Ndx     ,2)                                                            ;
    EY =zeros(Ndy  ,Ndx     ,2)                                                            ;
    HZ =zeros(Ndy  ,Ndx  ,3 ,2)                                                            ;
else
    if strcmp(PHASE,'ddmB') || strcmp(PHASE,'ddmE')
        l          =0:delta:L                                                              ;
        Ndx        =size(l,2)-1                                                            ;
        Ndy        =size(l,2)-1                                                            ;
        [Ex,Ey,Hz] =get2DfromV1D(INITIAL,LGBN,[])                                          ;
    end
end

if strcmp(PHASE,'fdtd') 
    list_glb_num=get_global_number_list([size(Ex,1),size(Ex,2)],SUBDOMDIV)                 ; % GET LIST OF GLOBAL NUMBERS
else
    if strcmp(PHASE,'ddmB') || strcmp(PHASE,'ddmE')
        list_glb_num=LGBN                                                                  ;
    end
end

MPex=zeros(size(Ex,1),size(Ex,2),2)                                                        ;
MPey=zeros(size(Ey,1),size(Ey,2),2)                                                        ;
MPhz=zeros(size(Hz,1),size(Hz,2),4)                                                        ;
Cex =zeros(size(Ex,1),size(Ex,2),3)                                                        ;
Cey =zeros(size(Ey,1),size(Ey,2),3)                                                        ;
Chz =zeros(size(Hz,1),size(Hz,2),8)                                                        ;
% MATERIAL PROPERTIES OF FREE SPACE / NON-PML REGION
MPex(:,:,1)=1*eps0                                                                         ;
MPey(:,:,1)=1*eps0                                                                         ;
MPhz(:,:,1)=1*mu0                                                                          ;

%%% GENERATE OBJECTS %%%
if SPEC(8)==1
    objclx=floor(SPEC(4)*L/delta)                                                          ;
    objcly=floor(SPEC(5)*L/delta)                                                          ;
    side=floor(((1/sqrt(2))*SPEC(9)*LAMmin)/delta)                                         ;
    if mode(side,2)==0
        OBJBx=objclx-side/2+1                                                              ;
        OBJEx=objclx+side/2-1                                                              ;
        OBJBy=objcly-side/2+1                                                              ;
        OBJEy=objcly+side/2-1                                                              ;
    else
        OBJBx=objclx-floor(side/2)+1                                                       ;
        OBJEx=objclx+floor(side/2)-1                                                       ;
        OBJBy=objcly-floor(side/2)+1                                                       ;
        OBJEy=objcly+floor(side/2)-1                                                       ;
    end
    MPex(OBJBx:OBJEx,OBJBy:OBJEy,2)=SIGMA                                                  ;
    MPey(OBJBx:OBJEx,OBJBy:OBJEy,2)=SIGMA                                                  ;
else
    if SPEC(8)==2
        objclx=floor(SPEC(4)*L/delta)                                                      ;
        objcly=floor(SPEC(5)*L/delta)                                                      ;
        for i=1:size(MPex,1)
            for j=1:size(MPex,2)
                if (i-objclx)^2+(j-objcly)^2<=((SPEC(9)*LAMmin)/delta)^2
                    MPex(i,j,2)=SIGMA                                                      ;
                    MPey(i,j,2)=SIGMA                                                      ;
                end
            end
        end
    else
        if SPEC(8)==3            
            objclx=floor(SPEC(4)*L/delta)                                                  ;
            objcly=floor(SPEC(5)*L/delta)                                                  ;
            for i=1:size(MPex,1)
                for j=1:size(MPex,2)
                    if (i-objclx)^2+(j-objcly)^2<=((SPEC(9)*LAMmin)/delta)^2
                        MPex(i,j,2)=SIGMA                                                  ;
                        MPey(i,j,2)=SIGMA                                                  ;
                    end
                end
            end
            objclx=floor(SPEC(6)*L/delta)                                                  ;
            objcly=floor(SPEC(7)*L/delta)                                                  ;
            for i=1:size(MPex,1)
                for j=1:size(MPex,2)
                    if (i-objclx)^2+(j-objcly)^2<=((SPEC(10)*LAMmin)/delta)^2
                        MPex(i,j,2)=SIGMA                                                  ;
                        MPey(i,j,2)=SIGMA                                                  ;
                    end
                end
            end
        end
    end
end
MPEX=MPex                                                                                  ; % FOR DATA EXPORT
   
% MATERIAL PROPERTIES OF PML REGION
ib=2                                                                                       ;
ie=NPMLC+1                                                                                 ;
for i=ib:ie
    MPey(:,i,2)  =SIGMAMAX.*((1/(ie-ib))*(-i+ie))^Npml                                     ;
    MPhz(:,i-1,3)=(mu0/eps0)*MPey(1,i,2)                                                   ;
    MPex(i,:,2)  =SIGMAMAX.*((1/(ie-ib))*(-i+ie))^Npml                                     ;
    MPhz(i-1,:,4)=(mu0/eps0)*MPex(i,1,2)                                                   ;
end
ib=size(MPex,1)-NPMLC                                                                      ;
ie=size(MPex,1)-1                                                                          ;
for i=ib:ie    
    MPex(i,:,2)=SIGMAMAX.*((1/(ie-ib))*(i-ib))^Npml                                        ;
    MPhz(i,:,4)=(mu0/eps0)*MPex(i,1,2)                                                     ;
end
ib=size(MPey,2)-NPMLC                                                                      ;
ie=size(MPey,2)-1                                                                          ;
for i=ib:ie 
    MPey(:,i,2)=SIGMAMAX.*((1/(ie-ib))*(i-ib))^Npml                                        ;
    MPhz(:,i,3)=(mu0/eps0)*MPey(1,i,2)                                                     ;
end

% FIELD COEFFICIENT VALUES 
Cex(:,:,1)=(2.*MPex(:,:,1)-dt.*MPex(:,:,2))./...
    (2.*MPex(:,:,1)+dt.*MPex(:,:,2))                                                       ;
Cex(:,:,2)=(2*dt)./...
    ((2.*MPex(:,:,1)+dt.*MPex(:,:,2)).*delta)                                              ;
Cex(:,:,3)=-(2*dt)./...
    (2.*MPex(:,:,1)+dt.*MPex(:,:,2))                                                       ;
Cey(:,:,1)=(2.*MPey(:,:,1)-dt.*MPey(:,:,2))./...
    (2.*MPey(:,:,1)+dt.*MPey(:,:,2))                                                       ;
Cey(:,:,2)=-(2*dt)./...
    ((2.*MPey(:,:,1)+dt.*MPey(:,:,2)).*delta)                                              ;
Cey(:,:,3)=-(2*dt)./...
    (2.*MPey(:,:,1)+dt.*MPey(:,:,2))                                                       ;
Chz(:,:,1)=((2.*MPhz(:,:,1)-dt.*MPhz(:,:,2))./...
    (2.*MPhz(:,:,1)+dt.*MPhz(:,:,2)))                                                      ;
Chz(:,:,2)=(2*dt)./...
    ((2.*MPhz(:,:,1)+dt.*MPhz(:,:,2)).*delta)                                              ;
Chz(:,:,3)=-(2*dt)./...
    ((2.*MPhz(:,:,1)+dt.*MPhz(:,:,2)).*delta)                                              ;
Chz(:,:,4)=-(2*dt)./...
    (2.*MPhz(:,:,1)+dt.*MPhz(:,:,2))                                                       ;
Chz(:,:,5)=(2.*MPhz(:,:,1)-dt.*MPhz(:,:,3))./...
    (2.*MPhz(:,:,1)+dt.*MPhz(:,:,3))                                                       ;
Chz(:,:,6)=(2.*MPhz(:,:,1)-dt.*MPhz(:,:,4))./...
    (2.*MPhz(:,:,1)+dt.*MPhz(:,:,4))                                                       ;
Chz(:,:,7)=(2*dt)./...
    ((2.*MPhz(:,:,1)+dt.*MPhz(:,:,4)).*delta)                                              ;
Chz(:,:,8)=-(2*dt)./...
    ((2.*MPhz(:,:,1)+dt.*MPhz(:,:,3)).*delta)                                              ;

%%% FDTD UPDATE LOOP %%%
for T=1:Nt
    
    if strcmp(PHASE,'fdtd')
        Hz=fdtd_hz_update(Ex,Ey,Hz,Chz,Ndy,Ndx,NPMLC)                                      ;
    else
        if strcmp(PHASE,'ddmB')
            HzTRUE=fdtd_expo_ddm_B_hz(Ex,Ey,Hz,Chz,Ndy,Ndx,NPMLC,list_glb_num)             ;
        else
            if strcmp(PHASE,'ddmE')
                HzTRUE=fdtd_expo_ddm_E_hz(Ex,Ey,Hz,Chz,Ndy,Ndx,NPMLC,list_glb_num)         ;
            end
        end
    end

    %%% APPLY SOURCE
    if strcmp(PHASE,'fdtd')        
        if T<=TIMESTEP      
            Hz(PSFx,PSFy,1)=Hz(PSFx,PSFy,1)+Chz(PSFx,PSFy,4)*P(T)                          ;
        end        
    end
    
    if strcmp(PHASE,'fdtd')
        Ex=fdtd_ex_update(Ex,Hz,Cex,Ndy,Ndx);        
    else
        if strcmp(PHASE,'ddmB')
            Ex=fdtd_expo_ddm_B_ex(Ex,HzTRUE,Cex,Ndy,Ndx,list_glb_num)                      ;
        else
            if strcmp(PHASE,'ddmE')
                Ex=fdtd_expo_ddm_E_ex(Ex,HzTRUE,Cex,Ndy,Ndx,list_glb_num)                  ;
            end
        end
    end                
    
    if strcmp(PHASE,'fdtd')
        Ey=fdtd_ey_update(Ey,Hz,Cey,Ndy,Ndx)                                               ;
    else
        if strcmp(PHASE,'ddmB')
            Ey=fdtd_expo_ddm_B_ey(Ey,HzTRUE,Cey,Ndy,Ndx,list_glb_num)                      ;
        else
            if strcmp(PHASE,'ddmE')
                Ey=fdtd_expo_ddm_E_ey(Ey,HzTRUE,Cey,Ndy,Ndx,list_glb_num)                  ;
            end
        end
    end

    if strcmp(PHASE,'fdtd')        
        if T>=TIMESTEP
            EX(:,:  ,(T-TIMESTEP)+1)=Ex                                                    ;
            EY(:,:  ,(T-TIMESTEP)+1)=Ey                                                    ;
            HZ(:,:,:,(T-TIMESTEP)+1)=Hz                                                    ;
        end
    else
        if strcmp(PHASE,'ddmB') || strcmp(PHASE,'ddmE')
            EX=Ex                                                                          ;
            EY=Ey                                                                          ;
            HZ=Hz                                                                          ;
        end
    end
    
end

if strcmp(PHASE,'fdtd')
    vector_out=[]                                                                          ;
    for i=1:size(EX,3)
        vector_out=[vector_out,getV1Dfrom2D(EX(:,:,i),EY(:,:,i),HZ(:,:,:,i),list_glb_num)] ;
    end
else
    if strcmp(PHASE,'ddmB') || strcmp(PHASE,'ddmE')
        vector_out=getV1Dfrom2D(EX,EY,HZ,list_glb_num)                                     ;
        vector_out=(1/dt).*(vector_out-INITIAL)                                            ;
    end
end

end

