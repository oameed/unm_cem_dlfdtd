%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                     %%%
%%% COMPUTATIONAL EM LAB                         %%%
%%% DEEP LEARNING PROJECT                        %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD %%%
%%% FDTD / EXPO-DDM-FDTD DATA GENERATOR          %%%
%%% by: OAMEED NOAKOASTEEN, SHU WANG             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [y1,w]=rk4_expo(h,Y1,SPEC,list_glb_num)

BY1    = FDTD2DTEz(SPEC,[],'ddmB',Y1   ,list_glb_num,[])                              ;
EY1    = FDTD2DTEz(SPEC,[],'ddmE',Y1   ,list_glb_num,[])                              ;
BEY1   = FDTD2DTEz(SPEC,[],'ddmB',EY1  ,list_glb_num,[])                              ;
w      = Y1 +(h/2)*BY1                                                                ;
q      = EY1+(h/2)*BEY1                                                               ;
Bw     = FDTD2DTEz(SPEC,[],'ddmB',w    ,list_glb_num,[])                              ;
Bq     = FDTD2DTEz(SPEC,[],'ddmB',q    ,list_glb_num,[])                              ;
Y2     = w+(h/2)*q                                                                    ;
EY2    = FDTD2DTEz(SPEC,[],'ddmE',Y2   ,list_glb_num,[])                              ;
Y3     = w+(h/2)*EY2                                                                  ;
EY3    = FDTD2DTEz(SPEC,[],'ddmE',Y3   ,list_glb_num,[])                              ;
BEY3   = FDTD2DTEz(SPEC,[],'ddmB',EY3  ,list_glb_num,[])                              ;
EY2Y3  = FDTD2DTEz(SPEC,[],'ddmE',Y2+Y3,list_glb_num,[])                              ;
BEY2Y3 = FDTD2DTEz(SPEC,[],'ddmB',EY2Y3,list_glb_num,[])                              ;
Y4     = w+(h)*EY3+(h/2)*Bw+(h^2/2)*BEY3                                              ;
EY4    = FDTD2DTEz(SPEC,[],'ddmE',Y4   ,list_glb_num,[])                              ;
y1     = w+(h/6).*q+(h/3).*EY2Y3+(h/2).*Bw+(h^2/12).*Bq+(h^2/6).*BEY2Y3+(h/6).*EY4    ;

end

