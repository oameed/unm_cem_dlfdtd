#! /bin/bash

echo   ' ACTIVATING TENSORFLOW ENVIRONMENT '
source ~/anaconda3/etc/profile.d/conda.sh
conda  activate tfpy

cd run/fdtd

echo   ' GENERATING CONTINEOUS VIDEOS FOR EXPERIMENT v23 '
python predict.py -data type2 -net v23 -t 30 

echo   ' GENERATING GIFs FROM TEST       VIDEOS FOR EXPERIMENT v23 '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v23','TESTVID',3,'Power',0.5 ,0.05,1  );exit;"

echo   ' GENERATING GIFs FROM CONTINEOUS VIDEOS FOR EXPERIMENT v23 '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v23','CONTVID',3,'Power',0.5 ,0.05,0.5);exit;"


