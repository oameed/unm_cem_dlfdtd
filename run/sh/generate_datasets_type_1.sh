#! /bin/bash

echo ' ACTIVATING TENSORFLOW ENVIRONMENT '
source ~/anaconda3/etc/profile.d/conda.sh
conda  activate tfpy

cd run/fdtd

echo ' GENERATING DATASET type1'
matlab -nodisplay -nosplash -nodesktop -r "generator('type1',false,{});exit;"

echo ' PROCESSING DATASET type1'
python dataprocess.py -data type1 -simb 150

# THE SPEC FILE FOR 'type1' WAS GENERATED USING:
# generator('type1',true,{100,linspace(2*pi/16,6*pi/16,5),[],linspace(0.4,0.6,3),[0.4,0.5,0.6]})
# THE LIST OF FILE NAMES FOR TRAIN/TEST ASSIGNMENTS FOR 'type1' WERE GENERATED USING:
# python dataprocess.py -data type1 -simb 150 -list


