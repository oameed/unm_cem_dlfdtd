#! /bin/bash

echo ' ACTIVATING TENSORFLOW ENVIRONMENT '
source ~/anaconda3/etc/profile.d/conda.sh
conda  activate tfpy

cd run/fdtd

echo ' GENERATING DATASET type2'
matlab -nodisplay -nosplash -nodesktop -r "generator('type2',false,{});exit;"

echo ' PROCESSING DATASET type2'
python dataprocess.py -data type2

# THE SPEC FILE FOR 'type2' WAS GENERATED USING:
# generator('type2',true,{100,[],linspace(0.4,0.6,3),linspace(0.4,0.6,3),[0.4,0.5,0.6]})
# THE LIST OF FILE NAMES FOR TRAIN/TEST ASSIGNMENTS FOR 'type2' WERE GENERATED USING:
# python dataprocess.py -data type2 -list


