#! /bin/bash

echo   ' ACTIVATING TENSORFLOW ENVIRONMENT '
source ~/anaconda3/etc/profile.d/conda.sh
conda  activate tfpy

cd run/fdtdexpoddm

echo   ' GENEATE PREDICTIONS FOR STEP-2 OF EXPO-RK4 '
python predict.py -data type4 -net v41 -t 2 -c 5

echo   ' RUN EXPO-RK4 WITH PREDICTIONS FROM THE TRAINED NETWORK '
matlab -nodisplay -nosplash -nodesktop -r "expoddm('type4','v41',[4.1697e-05,4.1895e-05]);exit;"

echo   ' GENERATING GRAPHICS '
matlab -nodisplay -nosplash -nodesktop -r "graphics({'../../networks/v41/videos/expoddm/hdf','../../networks/v41/videos/expoddm/png'},'PD');exit;"


