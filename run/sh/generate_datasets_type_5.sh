#! /bin/bash

echo ' ACTIVATING TENSORFLOW ENVIRONMENT '
source ~/anaconda3/etc/profile.d/conda.sh
conda  activate tfpy

cd run/fdtdexpoddm

echo ' GENERATING DATASET type5'
matlab -nodisplay -nosplash -nodesktop -r "generator('type5',false,320:10:450,{},4);exit;"

echo ' PROCESSING DATASET type5'
python dataprocess.py -data type5

# THE SPEC FILE FOR 'type5' WAS GENERATED MANUALLY
# THE LIST OF FILE NAMES FOR TRAIN/TEST ASSIGNMENTS FOR 'type5' IS GENERATED MANUALLY


