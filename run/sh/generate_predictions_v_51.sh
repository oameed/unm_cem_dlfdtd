#! /bin/bash

echo   ' ACTIVATING TENSORFLOW ENVIRONMENT '
source ~/anaconda3/etc/profile.d/conda.sh
conda  activate tfpy

cd run/fdtdexpoddm

echo   ' ATTENTION: THIS EXPERIMENT USES type5 DATASET ON TRAINED NETWORK FROM EXPERIMENT v41 '

echo   ' MAKE DIRECTORY FOR EXPERIMENT v51'
rm     -rf                                ../../networks/v51
tar    -xvzf ../../networks/v00.tar.gz -C ../../networks
mv           ../../networks/v00           ../../networks/v51

echo   ' COPYING CHECKPOINTS FROM EXPERIMENT v41 TO EXPERIMENT v51 '
cp -r  ../../networks/v41/checkpoints ../../networks/v51

echo   ' GENEATE PREDICTIONS FOR STEP-2 OF EXPO-RK4 '
python predict.py -data type5 -net v51 -t 2 -c 5 -nointscale

echo   ' RUN EXPO-RK4 WITH PREDICTIONS FROM THE TRAINED NETWORK '
matlab -nodisplay -nosplash -nodesktop -r "expoddm('type5','v51',[2.2435e-05,2.2300e-05]);exit;"

echo   ' GENERATING GRAPHICS '
matlab -nodisplay -nosplash -nodesktop -r "graphics({'../../networks/v51/videos/expoddm/hdf','../../networks/v51/videos/expoddm/png'},'PD');exit;"


