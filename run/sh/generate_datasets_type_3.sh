#! /bin/bash

echo ' ACTIVATING TENSORFLOW ENVIRONMENT '
source ~/anaconda3/etc/profile.d/conda.sh
conda  activate tfpy

cd run/fdtd

echo ' GENERATING DATASET type3'
matlab -nodisplay -nosplash -nodesktop -r "generator('type3',false,{});exit;"

echo ' PROCESSING DATASET type3'
python dataprocess.py -data type3

# THE SPEC FILE FOR 'type3' WAS GENERATED USING:
# generator('type3',true,{100,[],linspace(0.4,0.6,3),[],[]})
# THE LIST OF FILE NAMES FOR TRAIN/TEST ASSIGNMENTS FOR 'type3' WERE GENERATED USING:
# python dataprocess.py -data type3 -list


