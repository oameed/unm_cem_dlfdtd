#! /bin/bash

echo ' ACTIVATING TENSORFLOW ENVIRONMENT '
source ~/anaconda3/etc/profile.d/conda.sh
conda  activate tfpy

cd run/fdtdexpoddm

echo ' GENERATING DATASET type4'
matlab -nodisplay -nosplash -nodesktop -r "generator('type4',false,180:5:250,{},2);exit;"

echo ' PROCESSING DATASET type4'
python dataprocess.py -data type4

# THE SPEC FILE FOR 'type4' WAS GENERATED USING:
# generator('type4',true,180:5:250,{[],[],[0.375,0.625],[0.375,0.625],[]},2)
# THE LIST OF FILE NAMES FOR TRAIN/TEST ASSIGNMENTS FOR 'type4' WERE GENERATED USING:
# python dataprocess.py -data type4 -list


