#! /bin/bash

echo   ' ACTIVATING TENSORFLOW ENVIRONMENT '
source ~/anaconda3/etc/profile.d/conda.sh
conda  activate tfpy

cd run/fdtd

echo   ' GENERATING CONTINEOUS VIDEOS FOR EXPERIMENT v13 '
python predict.py -data type1 -net v13 -t 25 

echo   ' GENERATING GIFs FROM TEST       VIDEOS FOR EXPERIMENT v13 '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v13','TESTVID',3,'Power',0.5 ,0.05,1);exit;"

echo   ' GENERATING GIFs FROM CONTINEOUS VIDEOS FOR EXPERIMENT v13 '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v13','CONTVID',3,'Power',0.5 ,0.05,1);exit;"


