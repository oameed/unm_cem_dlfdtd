################################
### UNIVERSITY OF NEW MEXICO ###
### COMPUTATIONAL EM LAB     ###
### DEEP LEARNING PROJECT    ###
### TRAINING/TESTING         ###
### by: OAMEED NOAKOASTEEN   ###
################################

import os
import sys
import numpy      as np
import tensorflow as tf
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
from paramd import PATHS,NETV,numT,numC,BATCH,OPTIM
from utilsd import getFILENAMES,cleanup,wHDF,write_summaries_image,ckptREAD,NaN_Inf_FLAGS
from inputd import fetchSHAPE,saveTFRECORDS,inputTFRECORDS,TESTVIDSTFRECORDS
from lossd  import loss
sys.path.append(os.path.join(sys.path[0],'..','..','networks',NETV,'model'))
from nnd    import NET
from tensorflow.core.protobuf import rewriter_config_pb2              # !!!

TFRTRAIN    =os.path.join(PATHS[4],'train'    )
TFRTEST     =os.path.join(PATHS[4],'test'     )
LOGTRAIN    =os.path.join(PATHS[6],'train'    )
LOGTEST     =os.path.join(PATHS[6],'test'     )
LISTTRAIN   =os.path.join(PATHS[2],'listTRAIN')
LISTTEST    =os.path.join(PATHS[2],'listTEST' )
TWH         =fetchSHAPE  (PATHS[1]            )
SHAPE       =[numT,TWH[1],TWH[2],numC]                                # LIST 'SHAPE' IS THE SHAPE OF THE FIELD DATA IN A SINGLE EXAMPLE
TESTVIDBATCH=int(np.floor(TWH[0]/numT))

###########################
### CLEAN UP OPERATIONS ###
###########################
print(' CLEANING TFRECORDS TRAIN DIRECTORY ')
cleanup(TFRTRAIN) 
print(' CLEANING TFRECORDS TEST  DIRECTORY ')
cleanup(TFRTEST ) 
print(' CLEANING LOGS      TRAIN DIRECTORY ')
cleanup(LOGTRAIN) 
print(' CLEANING LOGS      TEST  DIRECTORY ')
cleanup(LOGTEST )  
print(' CLEANING CHECKPOINTS     DIRECTORY ')
cleanup(PATHS[5])
print(' CLEANING TEST VIDEO      DIRECTORY ')
cleanup(PATHS[7])

#######################
### WRITE TFRECORDS ###
#######################
print(' WRITING TRAINING DATA TO TFRECORDS FORMAT')
saveTFRECORDS(PATHS[1],[numT,numC],TFRTRAIN,LISTTRAIN) 
print(' WRITING TESTING  DATA TO TFRECORDS FORMAT')
saveTFRECORDS(PATHS[1],[numT,numC],TFRTEST ,LISTTEST )

################
### TRAINING ###
################
with tf.device('/gpu:0'): 
 vid_true,bnd=inputTFRECORDS(TFRTRAIN,SHAPE   ,OPTIM,BATCH,'train')   # INPUT PIPELINE
 vid_pred    =           NET(vid_true,bnd     ,            'train') 
 error       =          loss(vid_pred,vid_true,            'train')
 trainOP     =tf.compat.v1.train.AdamOptimizer().minimize(error)

write_summaries_image      ('IMAGES'+'_'+'INPUT'      +'_'+'train',tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[0][:,None,0,:,:,:]     )
write_summaries_image      ('IMAGES'+'_'+'BOUNDARY'   +'_'+'train',tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[1]                ,True)
write_summaries_image      ('IMAGES'+'_'+'OUTPUT'     +'_'+'train',tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[2][:,None,1,:,:,:]     )
tf.compat.v1.summary.scalar('Loss'  +'_'                  +'train',tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[3]                     )
saver        =tf.compat.v1.train.Saver(max_to_keep=OPTIM[3])          # DEFINE CHECKPOINT OPERATION  
summaryMERGED=tf.compat.v1.summary.merge_all()                        # DEFINE SUMMARY    OPERATIONS 
writer       =tf.compat.v1.summary.FileWriter(LOGTRAIN)
sess_config  =tf.compat.v1.ConfigProto(allow_soft_placement=True)     # DEFINE SESSION    CONFIGURATIONS
off          = rewriter_config_pb2.RewriterConfig.OFF                 # !!!
sess_config.graph_options.rewrite_options.memory_optimization=off     # !!!
print(' RUNNING TRAINING SESSION !')
with tf.compat.v1.Session(config=sess_config) as sess:
 sess.run(tf.compat.v1.global_variables_initializer())
 sess.run(tf.compat.v1.local_variables_initializer ()) 
 writer.add_graph(sess.graph)                                         # SUMMARY: WRITE GRAPH STRUCTURE 
 try:
  step=0
  while True:
   step=step+1   
   if step%OPTIM[2]==0:    
    summary=sess.run(summaryMERGED)                                   # SUMMARY: MERGE ALL SUMMARIES    
    writer.add_summary(summary,step)                                  # SUMMARY: WRITE SUMMARY TO FILE    
    saver.save(sess, os.path.join(PATHS[5],'model'),global_step=step) # CHECKPOINTING: WRITE CHECKPOINT TO FILE     
    print(' TRAINING IN PROGRESS!',' STEP: ',step)                    # ARE YOU STILL RUNNING ?!!!
   sess.run(trainOP)   
 except tf.errors.OutOfRangeError:
  print(' DONE TRAINING FOR %d EPOCHS, %d STEPS.' %(OPTIM[1],step))

###############
### TESTING ###
###############

tf.compat.v1.reset_default_graph()                                    # RESET GRAPH
ckpt         =os.path.join(PATHS[5],ckptREAD(PATHS[5],True))          # IMPORT GRAPH FROM THE APPROPRIATE CHECKPOINT
saver        =tf.compat.v1.train.import_meta_graph(ckpt+'.meta')
with tf.device('/gpu:0'): 
 vid_true,bnd=inputTFRECORDS(TFRTEST ,SHAPE   ,OPTIM,BATCH,'test')    # INPUT PIPELINE
 vid_pred    =           NET(vid_true,bnd     ,            'test')
 error       =          loss(vid_pred,vid_true,            'test')
write_summaries_image      ('IMAGES'+'_'+'INPUT'      +'_'+'test' ,tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[0][:,None,0,:,:,:],False,'test')
write_summaries_image      ('IMAGES'+'_'+'BOUNDARY'   +'_'+'test' ,tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[1]                ,True ,'test')
write_summaries_image      ('IMAGES'+'_'+'OUTPUT'     +'_'+'test' ,tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[2][:,None,1,:,:,:],False,'test')
tf.compat.v1.summary.scalar('Loss'  +'_'                  +'test' ,tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.ACTIVATIONS)[3]                             )
summaryMERGED=tf.compat.v1.summary.merge_all()                        # DEFINE SUMMARY    OPERATIONS 
writer       =tf.compat.v1.summary.FileWriter(LOGTEST)
sess_config  =tf.compat.v1.ConfigProto(allow_soft_placement=True)     # DEFINE SESSION CONFIGURATIONS
off          = rewriter_config_pb2.RewriterConfig.OFF                 # !!!
sess_config.graph_options.rewrite_options.memory_optimization=off     # !!!
print(' RUNNING TESTING SESSION !')
with tf.compat.v1.Session(config=sess_config) as sess:
 sess.run(tf.compat.v1.global_variables_initializer())
 sess.run(tf.compat.v1.local_variables_initializer ()) 
 saver.restore(sess,ckpt)                                             # RESTORE CHECKPOINT 
 writer.add_graph(sess.graph)                                         # SUMMARY: WRITE GRAPH STRUCTURE 
 try:
  step=0
  while True:
   step=step+1   
   if step%OPTIM[5]==0:    
    summary=sess.run(summaryMERGED)                                   # SUMMARY: MERGE ALL SUMMARIES    
    writer.add_summary(summary,step)                                  # SUMMARY: WRITE SUMMARY TO FILE    
    print(' TESTING IN PROGRESS!',' STEP: ',step)                     # ARE YOU STILL RUNNING?!!!
   sess.run(error)   
 except tf.errors.OutOfRangeError:
  print('DONE TESTING FOR %d EPOCH, %d STEPS.' %(OPTIM[4],step))

############################
### GENERATE TEST VIDEOS ###
############################

tf.compat.v1.reset_default_graph()                                    # RESET GRAPH
ckpt       =os.path.join(PATHS[5],ckptREAD(PATHS[5],True))            # IMPORT GRAPH FROM THE APPROPRIATE CHECKPOINT
saver      =tf.compat.v1.train.import_meta_graph(ckpt+'.meta')
filenames  =getFILENAMES(TFRTEST)                                     # COLLECT FILE NAMES
sess_config=tf.compat.v1.ConfigProto(allow_soft_placement=True)       # DEFINE SESSION CONFIGURATIONS
off        = rewriter_config_pb2.RewriterConfig.OFF                   # !!!
sess_config.graph_options.rewrite_options.memory_optimization=off     # !!!
print(' RUNNING TEST VIDEO GENERATION SESSION !')
with tf.compat.v1.Session(config=sess_config) as sess:
 sess.run(tf.compat.v1.global_variables_initializer())
 sess.run(tf.compat.v1.local_variables_initializer ()) 
 saver.restore(sess,ckpt)                                             # RESTORE CHECKPOINT 
 for index in range(len(filenames)):                                  # GENERATE VIDEOS
  filename      =filenames[index]
  _vid_true,_bnd=TESTVIDSTFRECORDS(filename ,SHAPE,TESTVIDBATCH)
  _vid_pred     =NET              (_vid_true,_bnd ,'test'      )
  try:
   while True:
    vid_true,bnd,vid_pred=sess.run([_vid_true,_bnd,_vid_pred])
                                                                      # RESHAPE TO FULL SIMULATION SIZE    
    shape                =vid_true.shape                                              
    vid_true             =np.reshape(vid_true,[shape[0]*shape[1],shape[2],shape[3],shape[4]])
    vid_pred             =np.reshape(vid_pred,[shape[0]*shape[1],shape[2],shape[3],shape[4]])
    bnd                  =np.reshape(bnd     ,[shape[0]*shape[1],shape[2],shape[3]         ])[0]
    
    NaN_Inf_FLAGS(vid_pred,filename,True)                             # CHECK FOR NaNs/Infs IN PREDICTED VIDEOS
                                                                      # WRITE TO HDF FILE    
    savefilename         =os.path.join(PATHS[7],filename.split('/')[6].split('.')[0]+'.h5')      
    wHDF(savefilename,
         ['vid_true','vid_pred','bnd'],
         [ vid_true , vid_pred , bnd ] )
  except tf.errors.OutOfRangeError:
   pass 


