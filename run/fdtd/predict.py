##############################################
### UNIVERSITY OF NEW MEXICO               ###
### COMPUTATIONAL EM LAB                   ###
### DEEP LEARNING PROJECT                  ###
### GENERATE CONTINEUOUS PREDICTION VIDEOS ###
### by: OAMEED NOAKOASTEEN                 ###
##############################################

import os
import sys
import numpy      as np
import tensorflow as tf
from matplotlib import pyplot as plt
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
from paramd import PATHS,NETV,numT,numC
from utilsd import cleanup,rFILE,rHDF,wHDF,ckptREAD,NaN_Inf_FLAGS
from inputd import fetchSHAPE,getCHANNELS
sys.path.append(os.path.join(sys.path[0],'..','..','networks',NETV,'model'))
from nnd    import NET

eta  =np.sqrt((4*np.pi*1e-7)/(8.85e-12))
TWH  =fetchSHAPE(PATHS[1])
SHAPE=[numT,TWH[1],TWH[2],numC]                                                                    # LIST 'SHAPE' IS THE SHAPE OF THE FIELD DATA IN A SINGLE EXAMPLE

###########################
### CLEAN UP OPERATIONS ###
###########################
print  (' CLEANING CONTINEOUS VIDEO PREDICTION DIRECTORY ')
cleanup(PATHS[8]                                          )   

###########################
### GET FULL FILE NAMES ###
###########################
filenames =[]
_filenames=rFILE (os.path.join(PATHS[2],'listTEST'   ))
for i in range(len(_filenames)):
 filenames.append(os.path.join(PATHS[1],_filenames[i]))

##################################
### GENERATE PREDICTION VIDEOS ###
##################################
ckpt       =os.path.join(PATHS[5],ckptREAD(PATHS[5],True))                                         # IMPORT GRAPH FROM THE APPROPRIATE CHECKPOINT
saver      =tf.train.import_meta_graph(ckpt+'.meta')
_vid       =tf.placeholder(tf.float32,shape=(1,SHAPE[0],SHAPE[1],SHAPE[2],SHAPE[3]))               # DEFINE NETWORK WITH PLACEHOLDERS 
_bnd       =tf.placeholder(tf.float32,shape=(1,SHAPE[0],SHAPE[1],SHAPE[2],1       ))
_vid_pred  =NET(_vid,_bnd,'test',True)
sess_config=tf.ConfigProto(allow_soft_placement=True)                                              # DEFINE SESSION CONFIGURATIONS
with tf.Session(config=sess_config) as sess:                                                       # RUN SESSION 
 sess.run(tf.global_variables_initializer())
 sess.run(tf.local_variables_initializer ())
 saver.restore(sess,ckpt)                                                                          # RUN: RESTORE CHECKPOINT
 for index in range(len(filenames)):                                                               # GENERATE VIDEOS
  vid_pred =np.array([])
  filename =filenames[index]  
  fobj,keys=rHDF(filename)                                                                         # GET TRUE VIDEO / LOAD 'numC' CHANNELS FROM FIELD DATA
  vid_true =getCHANNELS   (fobj['img'],SHAPE[3])
  bnd      =               fobj['bnd']  
  _vid_temp=np.reshape    (  vid_true  ,[int(TWH[0]/SHAPE[0]),SHAPE[0],SHAPE[1],SHAPE[2],SHAPE[3]])# GET FIRST VIDEO EXAMPLE FROM TRUE VIDEO DATA
  _vid_temp=_vid_temp[0][0]
  _vid_temp=np.stack      ([_vid_temp for _ in range(SHAPE[0])]                            ,axis=0)
  _vid_temp=np.expand_dims( _vid_temp                                                      ,axis=0)
  _bnd_temp=np.reshape    ( bnd        ,[int(TWH[0]/SHAPE[0]),SHAPE[0],SHAPE[1],SHAPE[2],1       ])
  _bnd_temp=np.expand_dims(_bnd_temp[0]                                                    ,axis=0)
  for i in range(int(TWH[0]/SHAPE[0])):
   temp       =sess.run  (_vid_pred,feed_dict={_vid:_vid_temp,_bnd:_bnd_temp})
   vid_pred   =np.append ( vid_pred,temp                                     )
   _vid_temp  =temp
  vid_pred    =np.reshape( vid_pred,[TWH[0],SHAPE[1],SHAPE[2],SHAPE[3]]      )  
  NaN_Inf_FLAGS(vid_pred,filename,True                                       )                     # CHECK FOR NaNs/Infs IN PREDICTED VIDEOS    
  savefilename=os.path.join(PATHS[8],filename.split('/')[6])                                       # WRITE TO HDF FILE
                                                                                                   # CORRECT THE DELAY BETWEEN TRUE AND CONT PREDICTION FRAMES
  wHDF(savefilename,
       ['vid_true','vid_pred',          'bnd'          ],
       [ vid_true , vid_pred ,np.squeeze(bnd,axis=3)[0]] )


