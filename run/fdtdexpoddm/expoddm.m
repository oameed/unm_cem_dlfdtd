%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                   %%%
%%% COMPUTATIONAL EM LAB                                       %%%
%%% DEEP LEARNING PROJECT                                      %%%
%%% DEEP LEARNING MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD %%%
%%% by: OAMEED NOAKOASTEEN, SHU WANG                           %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% SPEC(1)         : X-LOCATION OF THE SOURCE 
% SPEC(2)         : Y-LOCATION OF THE SOURCE
% SPEC(3): L      : LENGTH OF DOMAIN
%                   L = 0.96 --> 256x256
%                   L = 0.48 --> 128x128
% SPEC(4)         : TIME AT WHICH DOMAIN DECOMPOSITION STARTS
%                   L = 0.96 --> START:180 , STOP:260                   
% SPEC(5)         : X-LOCATION OF THE OBJECT
% SPEC(6)         : Y-LOCATION OF THE OBJECT
% SPEC(7)         : SIZE FACTOR OF THE OBJECT
% SOURCE(1)       : X-INDEX OF THE SOURCE 
% SOURCE(2)       : Y-INDEX OF THE SOURCE 
%                   L = 0.48 --> [64 ,51 ]
%                   L = 0.96 --> [128,102]
% SCALEFACTORS(1) : THE SCALE FACTOR FOR ALL FIELD COMPONENTS OF  Y1_fdtd
% SCALEFACTORS(2) : THE SCALE FACTOR FOR ALL FIELD COMPONENTS OF  w

function expoddm(DATA,NET,SCALEFACTORS)

close all
clc

addpath(         fullfile('..','..','data','solver','fdtdexpoddm'      ))                  ;

dt            = 8.83883476483184e-12                                                       ;
PATHS         = {fullfile('..','..','data'    ,DATA ,'hdf5'               ),...
                 fullfile('..','..','networks',NET  ,'videos','prediction'),...
                 fullfile('..','..','data'    ,DATA ,'info'               ),...
                 fullfile('..','..','networks',NET  ,'videos','expoddm'   )    }           ;
TIMEwFILENAME = fullfile(PATHS{3},strcat('timing','_','DL','.csv'))                        ;

filenames     =rFILE(strcat(PATHS{3},'/','listTEST'))                                      ;

for SIMULATION=1:size(filenames,2)-1
    FILENAME             =filenames(SIMULATION)                                            ;
    SPEC                 =get_spec(FILENAME,PATHS{3})                                      ;
    [glbnum             ,...
     Y1_fdtd            ,...
     y1_fdtd            ,...
     w_true             ,...
     y1_expoddm_analytic]  =get_TRUE_DATA(PATHS{1},filenames(SIMULATION))                  ;
    w_pred                 =get_PRED_DATA(PATHS{2},filenames(SIMULATION))                  ;
    w_pred                 =get_SCALED_DOWN_PRED(w_pred,SCALEFACTORS)                      ;
    w_pred                 =get_VECTORIZED_PREDS(w_pred,glbnum)                            ;
    disp(strcat(' BEGIN EXPO DDM FOR ',{' '},FILENAME{1}))
    tic                                                                                    ;
    y1_expoddm_deeplearning=RK4_EXPO(dt,Y1_fdtd,w_pred,SPEC,glbnum)                        ;
    time                   =toc                                                            ;
    disp(strcat(' END EXPO DDM FOR ',{' '},FILENAME{1}))    
    wHDF(fullfile(PATHS{4},'hdf',FILENAME{1})                             ,...
         {'Y1_fdtd','y1_fdtd','y1_expoddm_analytic','y1_expoddm_deeplearning','w_true','w_pred','glbnum','time'},...
         { Y1_fdtd , y1_fdtd , y1_expoddm_analytic , y1_expoddm_deeplearning , w_true , w_pred , glbnum , time }    )
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function filenames=rFILE(FILENAME)
        filenames           ={}                                                            ;
        index               =1                                                             ;
        FILE                =fopen(FILENAME,'r')                                           ;
        filenames{index}    =strtrim(string(fgets(FILE)))                                  ;
        while ~(strcmp(filenames{index},"-1"))
            index           =index+1                                                       ;
            filenames{index}=strtrim(string(fgets(FILE)))                                  ;
        end        
    end

    function SPEC=get_spec(FILENAME,INFOPATH)
        splt=split(FILENAME{1},'_')                                                        ;
        num =str2num(splt(2))                                                              ;
        spec=load(fullfile(INFOPATH,strcat('simspec','.csv')))                             ;
        SPEC=spec(num,:)                                                                   ;
    end
    
    function keys=rHDFkeys(FILENAME)
        keys=h5info(FILENAME)                                                              ;
        keys=keys.Datasets                                                                 ;
        keys=extractfield(keys,'Name')                                                     ;
    end

    function [glbnum,Y1_fdtd,y1_fdtd,w,y1]=get_TRUE_DATA(PATH,FILENAME)
        filename  =fullfile(PATH,'raw',FILENAME{1})                                        ;
        keys      =rHDFkeys(filename)                                                      ;
        glbnum    =h5read(filename,fullfile('/','glbnum'))                                 ;
        fdtd      =h5read(filename,fullfile('/','fdtd'  ))                                 ;
        Y1_fdtd   =fdtd(:,1)                                                               ;
        y1_fdtd   =fdtd(:,2)                                                               ;
        w         =h5read(filename,fullfile('/','w'     ))                                 ;
        y1        =h5read(filename,fullfile('/','y1'    ))                                 ;
    end

    function w=get_PRED_DATA(PATH,FILENAME)
        filename   =fullfile(PATH,'hdf',FILENAME{1})                                       ;
        keys       =rHDFkeys(filename)                                                     ;
        vid_pred   =h5read(filename,fullfile('/','vid_pred'))                              ;
        w          =squeeze(vid_pred(:,:,:,2,:))                                           ;
        w(isnan(w))=0                                                                      ;
    end

    function y1= RK4_EXPO (h,Y1,w,SPEC,list_glb_num)        
        EY1    = FDTD2DTEz(SPEC,[],'ddmE',Y1   ,list_glb_num)                              ;
        BEY1   = FDTD2DTEz(SPEC,[],'ddmB',EY1  ,list_glb_num)                              ;        
        q      = EY1+(h/2)*BEY1                                                            ;
        Bw     = FDTD2DTEz(SPEC,[],'ddmB',w    ,list_glb_num)                              ;
        Bq     = FDTD2DTEz(SPEC,[],'ddmB',q    ,list_glb_num)                              ;
        Y2     = w+(h/2)*q                                                                 ;
        EY2    = FDTD2DTEz(SPEC,[],'ddmE',Y2   ,list_glb_num)                              ;
        Y3     = w+(h/2)*EY2                                                               ;
        EY3    = FDTD2DTEz(SPEC,[],'ddmE',Y3   ,list_glb_num)                              ;
        BEY3   = FDTD2DTEz(SPEC,[],'ddmB',EY3  ,list_glb_num)                              ;
        EY2Y3  = FDTD2DTEz(SPEC,[],'ddmE',Y2+Y3,list_glb_num)                              ;
        BEY2Y3 = FDTD2DTEz(SPEC,[],'ddmB',EY2Y3,list_glb_num)                              ;
        Y4     = w+(h)*EY3+(h/2)*Bw+(h^2/2)*BEY3                                           ;
        EY4    = FDTD2DTEz(SPEC,[],'ddmE',Y4   ,list_glb_num)                              ;
        y1     = w+(h/6).*q+(h/3).*EY2Y3+(h/2).*Bw+(h^2/12).*Bq+(h^2/6).*BEY2Y3+(h/6).*EY4 ;
    end

    function W=get_SCALED_DOWN_PRED(W,SCALEFACTORS)
        ETA           =sqrt((4*pi*10^-7)/(8.85*10^-12))                                    ;
        W             =W.*SCALEFACTORS(2)                                                  ;
        W(3:end,:,:,:)=W(3:end,:,:,:)./ETA                                                 ;        
    end

    function vec=get_VECTORIZED_PREDS(VEC,GLBNUM)        
        vec       =zeros(prod(size(VEC)),1)                                                ;
        INDEX     =0                                                                       ;
        for subdom=1:size(VEC,4)
            %subdom_b_index=(subdom-1)*128*128                                              ;
            subdom_ex     = squeeze(VEC(1,:,:,subdom))                                     ;
            subdom_ey     = squeeze(VEC(2,:,:,subdom))                                     ;
            subdom_hz     = squeeze(VEC(3,:,:,subdom))                                     ;
            subdom_hzx    = squeeze(VEC(4,:,:,subdom))                                     ;
            subdom_hzy    = squeeze(VEC(5,:,:,subdom))                                     ;
            subdom_ex     = subdom_ex (:)                                                  ;
            subdom_ey     = subdom_ey (:)                                                  ;
            subdom_hz     = subdom_hz (:)                                                  ;
            subdom_hzx    = subdom_hzx(:)                                                  ;
            subdom_hzy    = subdom_hzy(:)                                                  ;
            %field_counter = subdom_b_index                                                 ;
            for index_fields=1:128*128
                vec(INDEX+1)=subdom_ex (index_fields)                                      ;
                vec(INDEX+2)=subdom_ey (index_fields)                                      ;
                vec(INDEX+3)=subdom_hz (index_fields)                                      ;
                vec(INDEX+4)=subdom_hzx(index_fields)                                      ;
                vec(INDEX+5)=subdom_hzy(index_fields)                                      ;
                INDEX               =INDEX+5                                               ;
                %field_counter       =field_counter+5                                       ;
            end
        end        
    end

    function wHDF(FILENAME,DIR,DATA)                
        for i=1:size(DIR,2)
            h5create(FILENAME,fullfile('/',DIR{i}),size(DATA{i}))                          ;
            h5write (FILENAME,fullfile('/',DIR{i}),     DATA{i} )                          ;
        end
    end
    
end

