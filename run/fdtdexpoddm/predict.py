###########################################################
### UNIVERSITY OF NEW MEXICO                            ###
### COMPUTATIONAL EM LAB                                ###
### DEEP LEARNING PROJECT                               ###
### GENERATE CONTINEUOUS PREDICTION FOR EXPONENTIAL DDM ###
### by: OAMEED NOAKOASTEEN                              ###
###########################################################

import os
import sys
import numpy      as np
import tensorflow as tf
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
from paramd import PATHS,NETV,numT,numC
from utilsd import cleanup,rFILE,rHDF,wHDF,ckptREAD,NaN_Inf_FLAGS
from inputd import fetchEXPODDMSHAPE,getEXPODDMDATASETS
sys.path.append(os.path.join(sys.path[0],'..','..','networks',NETV,'model'))
from nnd    import NET

DWHC =fetchEXPODDMSHAPE(PATHS[1])
SHAPE=[numT,DWHC[1],DWHC[2],numC]                                                                  # LIST 'SHAPE' IS THE SHAPE OF THE FIELD DATA IN A SINGLE EXAMPLE

###########################
### CLEAN UP OPERATIONS ###
###########################
print  (' CLEANING CONTINEOUS VIDEO PREDICTION DIRECTORY ')
cleanup(PATHS[8]                                          ) 

##################################
### GENERATE PREDICTION VIDEOS ###
##################################
filenames  =[os.path.join(PATHS[1],FILE) for FILE in rFILE(os.path.join(PATHS[2],'listTEST'))]
ckpt       =os.path.join(PATHS[5],ckptREAD(PATHS[5],True))                                         # IMPORT GRAPH FROM THE APPROPRIATE CHECKPOINT
saver      =tf.compat.v1.train.import_meta_graph(ckpt+'.meta')
_vid       =tf.compat.v1.placeholder(tf.float32,shape=(1,SHAPE[0],SHAPE[1],SHAPE[2],SHAPE[3]))     # DEFINE NETWORK WITH PLACEHOLDERS 
_bnd       =tf.compat.v1.placeholder(tf.float32,shape=(1,SHAPE[0],SHAPE[1],SHAPE[2],1       ))
_vid_pred  =NET(_vid,_bnd,'test')
sess_config=tf.compat.v1.ConfigProto(allow_soft_placement=True)                                    # DEFINE SESSION CONFIGURATIONS
with tf.compat.v1.Session(config=sess_config) as sess:                                             # RUN SESSION
 sess.run(tf.compat.v1.global_variables_initializer())
 sess.run(tf.compat.v1.local_variables_initializer ())
 saver.restore(sess,ckpt)                                                                          # RUN: RESTORE CHECKPOINT 
 for index in range(len(filenames)):                                                               # GENERATE VIDEOS
  vid_pred    =[]
  img         =getEXPODDMDATASETS(filenames[index],'fields'         )                              # GET TRUE VIDEO / LOAD 'numC' CHANNELS FROM FIELD DATA
  bnd         =getEXPODDMDATASETS(filenames[index],'bnd'   ,SHAPE[0])  
  for subdom in range(img.shape[0]):
   vid_pred.append(sess.run(_vid_pred,feed_dict={_vid:img[None,subdom],_bnd:bnd[None,subdom]}))
  vid_pred    =np.squeeze(np.array(vid_pred),axis=1)
  NaN_Inf_FLAGS(vid_pred,filenames[index],True)  
  wHDF(os.path.join(PATHS[8],filenames[index].split('/')[6]),
       ['vid_true','vid_pred','bnd']                        ,
       [ img      , vid_pred , bnd ]                         )

  
