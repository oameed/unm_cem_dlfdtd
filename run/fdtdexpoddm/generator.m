%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                     %%%
%%% COMPUTATIONAL EM LAB                         %%%
%%% DEEP LEARNING PROJECT                        %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD %%%
%%% FDTD / EXPO-DDM-FDTD DATA GENERATOR          %%%
%%% by: OAMEED NOAKOASTEEN, SHU WANG             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TYPE         : 'type4'
% GENSPEC      : IF TRUE, GENERATE NEW SPEC FILE
% FRAMES       : TIME AT WHICH DOMAIN DECOMPOSITION STARTS
% SPECPARAMS(1): TOTAL NUMBER OF SIMULATIONS
% SPECPARAMS(2): RANGE OF VALUES FOR ANGLE OF PROPAGATION OF THE TFSF SOURCE
% SPECPARAMS(3): RANGE OF VALUES FOR POINT SOURCE LOCATIONS
% SPECPARAMS(4): RANGE OF VALUES FOR OBJECT       LOCATIONS
% SPECPARAMS(5): RANGE OF VALUES FOR OBJECT SIZES
% SUBDOMDIV    : NUMBER OF SUBDOMAIN DIVISIONS
% SPEC(1)      : ANGLE OF PROPAGATION OF THE TFSF         SOURCE
% SPEC(2)      : X-LOCATION           OF THE FIRST  POINT SOURCE 
% SPEC(3)      : Y-LOCATION           OF THE FIRST  POINT SOURCE
% SPEC(4)      : X-LOCATION           OF THE SECOND POINT SOURCE
% SPEC(5)      : Y-LOCATION           OF THE SECOND POINT SOURCE
% SPEC(6): L   : LENGTH OF DOMAIN
%                 L = 3.84 --> 1024 x 1024
%                 L = 1.92 --> 512  x 512
%                 L = 0.96 --> 256  x 256  T=[180,250]
%                 L = 0.48 --> 128  x 128
% SPEC(7)      : X-LOCATION  OF THE FIRST  OBJECT
% SPEC(8)      : Y-LOCATION  OF THE FIRST  OBJECT
% SPEC(9)      : X-LOCATION  OF THE SECOND OBJECT
% SPEC(10)     : Y-LOCATION  OF THE SECOND OBJECT
% SPEC(11)     : OBJECT TYPE AND NUMBER
%                 '1': SQUARE
%                 '2': CIRCLE
%                 '3': MIX OF BOTH
% SPEC(12)     : SIZE FACTOR OF THE FIRST  OBJECT
% SPEC(13)     : SIZE FACTOR OF THE SECOND OBJECT

function generator(TYPE,SPECLIST,FRAMES,SPECPARAMS,SUBDOMDIV)
close all 
clc

PATHS       ={fullfile('..' ,'..','data',TYPE,'info'      ),...
              fullfile('..' ,'..','data',TYPE,'hdf5','raw'),...
              fullfile('..' ,'..','data',TYPE,'png' ,'raw')    }                         ;
specfilename= fullfile(PATHS{1}  ,strcat('simspec','.csv'))                              ;

if SPECLIST
    specs    =get_specs(TYPE,SPECPARAMS)                                                 ;
    writematrix(specs,specfilename)
    disp([' GENERATED SPEC FILE FOR ',' ',TYPE])
else
    disp([' READING   SPEC FILE FOR ',' ',TYPE])
    specs    =load(specfilename)                                                         ;
end

for sim=1:size(specs,1)
    TIME=[]                                                                              ;
    SPEC=map_specs_to_SPEC(specs(sim,:))                                                 ;
    for T=1:size(FRAMES,2)
        disp([' GENERATING ',strcat('simulation','_',num2str(sim),'_',num2str(FRAMES(T)),'.h5')])
        time=datagen_process(TYPE,sim,SPEC,FRAMES(T),SUBDOMDIV)                                    ;
        TIME=[TIME;time]                                                                 ;
    end
    writematrix(TIME,fullfile(PATHS{1},strcat('timing','.csv')))
end

disp(' GENERATING FIGURES ')

addpath(fullfile('..','..','lib','fdtdexpoddm'))
Rfilenames=get_r_FILENAMES(PATHS{2}           )                                          ;
Rfilenames=fullfile(       PATHS{2},Rfilenames)                                          ;
for index=1:size(Rfilenames,2)
    [Wfilename,tags]                  =get_w_FILENAME(PATHS{3},Rfilenames{index})        ;
    [Y1,~,glbnum,y1_fdtd,y1_]         =rHDF(Rfilenames{index})                           ;
    [     Y1_EX,     Y1_EY,     Y1_HZ]=get2DfromV1D(Y1     ,glbnum,[])                   ;
    [     y1_EX,     y1_EY,     y1_HZ]=get2DfromV1D(y1_    ,glbnum,[])                   ;
    [y1_fdtd_EX,y1_fdtd_EY,y1_fdtd_HZ]=get2DfromV1D(y1_fdtd,glbnum,[])                   ;
    plotter({      Y1_EX,    Y1_EY,     Y1_HZ(:,:,1) ,...
                   y1_EX,    y1_EY,     y1_HZ(:,:,1) ,...
             y1_fdtd_EX,y1_fdtd_EY,y1_fdtd_HZ(:,:,1)},...
              Wfilename,tags{3}                          )
end

disp(' FINISHED ')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function spec=get_specs(TYPE,SPECPARAMS)
        spec    =[]                                                                      ;
        numtotal=SPECPARAMS{1}                                                           ;
        if strcmp(TYPE,'type4')
            source_point_pos=SPECPARAMS{3}                                               ; 
            obj_pos         =SPECPARAMS{4}                                               ;
            for index1=1:size(source_point_pos,2)
                for index2=size(source_point_pos,2)
                    for index3=1:size(obj_pos,2)
                        for index4=1:size(obj_pos,2)
                            x   =[source_point_pos(index1),...
                                  source_point_pos(index2),...
                                  obj_pos(index3)         ,...
                                  obj_pos(index4)             ]                          ;
                            x   =[0,x(1),x(2),0,0,0.96,x(3),x(4),0,0,2,0.5,0]            ;
                            spec=[spec;x]                                                ;
                        end
                    end
                end
            end
            if isempty(numtotal)
                numtotal=size(spec,1)                                                    ;
            end
            spec  =spec(randperm(size(spec,1),numtotal),:)                               ;
            index5=[]                                                                    ;
            for index6=1:size(spec,1)
                if isequal([spec(index6,2),spec(index6,3)],[spec(index6,7),spec(index6,8)])
                    index5=[index5,index6]                                               ;
                end
            end
            if ~ isempty(index5)
                spec(index5,:)=[]                                                        ;
            end
        end
    end

    function SPEC=map_specs_to_SPEC(specs)
        SPEC=[specs(2),specs(3 ),specs(6 ),specs(7 ),specs(8 ),...
              specs(9),specs(10),specs(11),specs(12),specs(13)]                          ;        
    end

    function  time=datagen_process(TYPE,SIMINDEX,SPEC,TIME,SUBDOMDIV)
        dt          = 8.83883476483184e-12                                               ;
        addpath(     fullfile('..','..','data','solver','fdtdexpoddm'      ))            ;
        savePATH    =fullfile('..','..','data',TYPE    ,'hdf5'       ,'raw')             ;
        saveFILENAME=fullfile(savePATH,strcat('simulation'     ,'_'  ,...
                                              num2str(SIMINDEX),'_'  ,...
                                              num2str(TIME)    ,'.h5'    ))              ;
        
        [fdtd_vector_out,list_glb_num,MPex]=FDTD2DTEz(SPEC,TIME,'fdtd',[],[],SUBDOMDIV)  ;
        disp(' FDTD INITIAL FRAME ACQUIRED ')
        tic                                                                              ;
        [y1,W]               = rk4_expo(dt,fdtd_vector_out(:,1),SPEC,list_glb_num)       ;        
        time                 = toc                                                       ;
        disp(' EXPO-RK4 FINISHED '          )
        wHDF(saveFILENAME                                    ,...
             {'fdtd'          ,'y1','w','glbnum'     ,'MPex'},...
             { fdtd_vector_out, y1 , W , list_glb_num, MPex }    )     
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% FUNCTION DEFINITIONS %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function wHDF(FILENAME,DIR,DATA)
            for i=1:size(DIR,2)
                h5create(FILENAME,fullfile('/',DIR{i}),size(DATA{i}))                    ;
                h5write (FILENAME,fullfile('/',DIR{i}),     DATA{i} )                    ;
            end
        end
        
    end

    function list=get_r_FILENAMES(PATH)
        DIR =dir(PATH)                                                                   ;
        list={DIR(4:end).name}                                                           ;
    end

    function tag=get_w_FILENAME_TAG(FILENAME)
        tag=split(FILENAME,'/')                                                          ;
        tag=tag{end}                                                                     ;
        tag=split(tag,'.')                                                               ;
        tag=tag{1}                                                                       ;
        tag=split(tag,'_')                                                               ;
    end

    function [filename,tags]=get_w_FILENAME(PATH,FILENAME)
        tags    =get_w_FILENAME_TAG(FILENAME)                                            ;
        filename=fullfile(PATH,strcat(tags{1},'_',tags{2},'_',tags{3},'.png'))           ;
    end

    function [Y1,w,glbnum,y1_fdtd,y1]=rHDF(FILENAME)        
        fdtd   =h5read(FILENAME,fullfile('/','fdtd'  ))                                  ;
        Y1     =fdtd(:,1)                                                                ;
        w      =h5read(FILENAME,fullfile('/','w'     ))                                  ;
        y1     =h5read(FILENAME,fullfile('/','y1'    ))                                  ;
        y1_fdtd=fdtd(:,2)                                                                ;
        glbnum =h5read(FILENAME,fullfile('/','glbnum'))                                  ;
    end

    function plot_config(DATA,NUMCOL,SUBPLOTNUM,LABEL)
        subplot(4,NUMCOL,SUBPLOTNUM)
        surf(DATA)
        xlabel(LABEL)
        set(gca,'xticklabel',[],'yticklabel',[])
        axis    equal        
        shading interp        
        view(2)        
    end    

    function plotter(DATA,FILENAME,TAG)
        close all        
        SIZE_SCALE  =1.25                                                                ;
        h           =figure('Position',[520  378 SIZE_SCALE*560  SIZE_SCALE*420])        ;
        sgt         =sgtitle({'University of New Mexico','Computational EM Lab',...
                              strcat('Time-Step :'," ",TAG)})                            ;
        sgt.FontSize=10                                                                  ;
        Y1_EX_       =DATA{1}                                                            ;
        Y1_EY_       =DATA{2}                                                            ;
        Y1_HZ_       =DATA{3}                                                            ;
        y1_EX_       =DATA{4}                                                            ;
        y1_EY_       =DATA{5}                                                            ;
        y1_HZ_       =DATA{6}                                                            ;
        y1_fdtd_EX_  =DATA{7}                                                            ;
        y1_fdtd_EY_  =DATA{8}                                                            ;
        y1_fdtd_HZ_  =DATA{9}                                                            ;
        power_Y1     =0.5.*abs(Y1_HZ_)     .*sqrt(Y1_EX_     .^2+Y1_EY_     .^2)         ;
        power_y1_fdtd=0.5.*abs(y1_HZ_)     .*sqrt(y1_EX_     .^2+y1_EY_     .^2)         ;
        power_y1     =0.5.*abs(y1_fdtd_HZ_).*sqrt(y1_fdtd_EX_.^2+y1_fdtd_EY_.^2)         ;
        plot_config(Y1_EX_  ,3,1 ,'Ex'                                         )
        plot_config(Y1_EY_  ,3,4 ,'Ey'                                         )
        plot_config(Y1_HZ_  ,3,7 ,'Hz'                                         )
        plot_config(power_Y1,3,10,{'Power',...
                                   'FDTD' ,...
                                   strcat('Step:'," ",TAG)}                    )        
        plot_config(y1_fdtd_EX_  ,3,2 ,'Ex'                                    )
        plot_config(y1_fdtd_EY_  ,3,5 ,'Ey'                                    )
        plot_config(y1_fdtd_HZ_  ,3,8 ,'Hz'                                    )
        plot_config(power_y1_fdtd,3,11,{'Power',...
                                   'FDTD' ,...
                                   strcat('Step:'," ",num2str(str2num(TAG)+1))})                               
        plot_config(y1_EX_  ,3,3 ,'Ex'                                         )
        plot_config(y1_EY_  ,3,6 ,'Ey'                                         )
        plot_config(y1_HZ_  ,3,9 ,'Hz'                                         )
        plot_config(power_y1,3,12,{'Power',...
                                   'EXPONENTIAL-DDM-FDTD' ,...
                                   strcat('Step:'," ",num2str(str2num(TAG)+1))})
        saveas(gcf,FILENAME,'png')
        close all
    end

end

