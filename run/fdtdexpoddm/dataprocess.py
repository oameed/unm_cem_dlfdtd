################################################
### UNIVERSITY OF NEW MEXICO                 ###
### COMPUTATIONAL EM LAB                     ###
### DEEP LEARNING PROJECT                    ###
### PRE-PROCESSING EXPONENTIAL DDM RAW DATA  ###
### by: OAMEED NOAKOASTEEN                   ###
################################################

import os
import sys
import numpy as np

sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
from paramd import PATHS          ,genLIST         ,trainDIV
from utilsd import getFILENAMES   ,wHDF
from datad  import genRNDlist     ,getEXPODDMSCALE,getEXPODDMDATA

RESULTFILENAME=os.path.join(PATHS[2],'log-processing_raw')
filenames     =getFILENAMES(PATHS[0])

#################################
### GENERATE TRAIN/TEST LISTS ###
#################################
if genLIST:
 genRNDlist(PATHS[0],PATHS[2],trainDIV,True)

####################################
### GET APPROPRIATE SCALE FACTOR ###
####################################
SCALES        =getEXPODDMSCALE(filenames)
scale_Y1_fdtd =0.5*(SCALES[0]+SCALES[1])
scale_w       =0.5*(SCALES[2]+SCALES[3])
print(' THE MAX          OF  E-FIELD MAGNITUDE    FOR Y1_fdtd IS ',"{0:.4e}".format(SCALES[0]    ),file=open(RESULTFILENAME,'w'))
print(' THE MAX          OF  H-FIELD MAGNITUDE    FOR Y1_fdtd IS ',"{0:.4e}".format(SCALES[1]    ),file=open(RESULTFILENAME,'a'))
print(' THE SCALE FACTOR FOR ALL FIELD COMPONENTS OF  Y1_fdtd IS ',"{0:.4e}".format(scale_Y1_fdtd),file=open(RESULTFILENAME,'a'))
print(' THE MAX          OF  E-FIELD MAGNITUDE    FOR w       IS ',"{0:.4e}".format(SCALES[2]    ),file=open(RESULTFILENAME,'a'))
print(' THE MAX          OF  H-FIELD MAGNITUDE    FOR w       IS ',"{0:.4e}".format(SCALES[3]    ),file=open(RESULTFILENAME,'a'))
print(' THE SCALE FACTOR FOR ALL FIELD COMPONENTS OF  w       IS ',"{0:.4e}".format(scale_w      ),file=open(RESULTFILENAME,'a'))

########################
### PROCESS RAW DATA ###
########################
for i in range(len(filenames)):
 Y1_fdtd,w,bnd,Y1_fdtd_MAX,w_MAX=getEXPODDMDATA(filenames[i],[scale_Y1_fdtd,scale_w])  
 SAVEFILENAME                   =os.path.join(PATHS[1],filenames[i].split('/')[6]) 
 wHDF(SAVEFILENAME,
      ['fdtd' ,'w','bnd'],
      [Y1_fdtd, w , bnd ])
 print(' PROCESSED '           ,filenames[i].split('/')[6],
       ' Y1_fdtd MAX MAG E IS ',"{0:.4e}".format(Y1_fdtd_MAX[0]),
       ' Y1_fdtd MAX MAG H IS ',"{0:.4e}".format(Y1_fdtd_MAX[1]),
       ' W MAX MAG E IS '      ,"{0:.4e}".format(w_MAX      [0]),
       ' W MAX MAG H IS '      ,"{0:.4e}".format(w_MAX      [1]),
       file=open(RESULTFILENAME,'a')                             )


