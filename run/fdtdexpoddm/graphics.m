%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                     %%%
%%% COMPUTATIONAL EM LAB                         %%%
%%% DEEP LEARNING PROJECT                        %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD %%%
%%% GRAPHICS                                     %%%
%%% by: OAMEED NOAKOASTEEN, SHU WANG             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% PATH{1}:         PATH TO READ DATA
% PATH{2}:         PATH TO SAVE DATA
% MODE   :         TD: TRUE DATA / PD: PREDICTED DATA

function graphics(PATH,MODE)
 clc
 addpath(fullfile('..','..','lib','fdtdexpoddm')) 

 rPATH     =fullfile(pwd,PATH{1})                                                                  ;
 wPATH     =fullfile(pwd,PATH{2})                                                                  ;
 Rfilenames=get_r_FILENAMES(rPATH)                                                                 ;
 Rfilenames=fullfile(rPATH,Rfilenames)                                                             ;
 
 for i=1:size(Rfilenames,2)
     [Wfilename,tags]                  =get_w_FILENAME(wPATH,Rfilenames{i})                        ;
     [Y1,~,glbnum,y1_fdtd,y1]          =rHDF(Rfilenames{i},MODE)                                   ;

     [     Y1_EX,     Y1_EY,     Y1_HZ]=get2DfromV1D(Y1     ,glbnum,[]  )                          ;
     [y1_fdtd_EX,y1_fdtd_EY,y1_fdtd_HZ]=get2DfromV1D(y1_fdtd,glbnum,[]  )                          ;
     [     y1_EX,     y1_EY,     y1_HZ]=get2DfromV1D(y1     ,glbnum,true)                          ;

     [     y1_EX,     y1_EY,     y1_HZ]=set_scales({y1_fdtd_EX,y1_fdtd_EY,y1_fdtd_HZ(:,:,1)},...
                                                   {     y1_EX,     y1_EY,     y1_HZ(:,:,1)})      ;

     plotter({     Y1_EX,     Y1_EY,     Y1_HZ(:,:,1) ,...
              y1_fdtd_EX,y1_fdtd_EY,y1_fdtd_HZ(:,:,1) ,...
                   y1_EX,     y1_EY,     y1_HZ       },...
              Wfilename ,tags{3}                          )
 end
 
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%% FUNCTION DEFINITIONS %%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
    function list=get_r_FILENAMES(PATH)        
        DIR =dir(PATH)                                                                             ;
        list={DIR(4:end).name}                                                                     ;
    end

    function tag=get_w_FILENAME_TAG(FILENAME)
        tag=split(FILENAME,'/')                                                                    ;
        tag=tag{end}                                                                               ;
        tag=split(tag,'.')                                                                         ;
        tag=tag{1}                                                                                 ;
        tag=split(tag,'_')                                                                         ;        
    end

    function [filename,tags]=get_w_FILENAME(PATH,FILENAME)
        tags    =get_w_FILENAME_TAG(FILENAME)                                                      ;
        filename=fullfile(PATH,strcat(tags{1},'_',tags{2},'_',tags{3},'.png'))                     ;
    end

    function [Y1,w,glbnum,y1_fdtd,y1]=rHDF(FILENAME,MODE)
        if strcmp(MODE,'TD')
            fdtd   =h5read(FILENAME,fullfile('/','fdtd'  ))                                        ;
            Y1     =fdtd(:,1)                                                                      ;
            w      =h5read(FILENAME,fullfile('/','w'     ))                                        ;
            y1     =h5read(FILENAME,fullfile('/','y1'    ))                                        ;
            y1_fdtd=fdtd(:,2)                                                                      ;
            glbnum =h5read(FILENAME,fullfile('/','glbnum'))                                        ;
        else
            if strcmp(MODE,'PD')
                Y1                     =h5read(FILENAME,fullfile('/','Y1_fdtd'                ))   ;
                y1_fdtd                =h5read(FILENAME,fullfile('/','y1_fdtd'                ))   ;
                y1_expoddm_analytic    =h5read(FILENAME,fullfile('/','y1_expoddm_analytic'    ))   ;
                y1_expoddm_deeplearning=h5read(FILENAME,fullfile('/','y1_expoddm_deeplearning'))   ;
                w                      =h5read(FILENAME,fullfile('/','w_pred'                 ))   ;
                glbnum                 =h5read(FILENAME,fullfile('/','glbnum'                 ))   ;
                y1                     =y1_expoddm_deeplearning                                    ;
            end
        end
    end

    function [MIN,MAX]=get_min_max(SUBDOM)
        MIN=min(min(SUBDOM))                                                                       ;
        MAX=max(max(SUBDOM))                                                                       ;
    end

    function SUBDOM=set_min_max(SUBDOM,PARAMS)
        MIN_TRUE =PARAMS{1}                                                                        ;
        MAX_TRUE =PARAMS{2}                                                                        ;
        [min,max]=get_min_max(SUBDOM)                                                              ;
        SUBDOM   =(SUBDOM-min)./(max-min)                                                          ;
        SUBDOM   =SUBDOM.*(MAX_TRUE-MIN_TRUE)+MIN_TRUE                                             ;        
    end

    function PRED=apply_scales(TRUE,PRED)
        if size(TRUE,1)==256
            [TRUE_1_min,TRUE_1_max]=get_min_max( TRUE(1  :128,1  :128))                            ;
            [TRUE_2_min,TRUE_2_max]=get_min_max( TRUE(129:256,1  :128))                            ;
            [TRUE_3_min,TRUE_3_max]=get_min_max( TRUE(1  :128,129:256))                            ;
            [TRUE_4_min,TRUE_4_max]=get_min_max( TRUE(129:256,129:256))                            ;        
            PRED(1  :128,1  :128)  =set_min_max( PRED(1  :128,1  :128)  ,...
                                                {TRUE_1_min,TRUE_1_max})                           ;
            PRED(129:256,1  :128)  =set_min_max( PRED(129:256,1  :128)  ,...
                                                {TRUE_2_min,TRUE_2_max})                           ;
            PRED(1  :128,129:256)  =set_min_max( PRED(1  :128,129:256)  ,...
                                                {TRUE_3_min,TRUE_3_max})                           ;
            PRED(129:256,129:256)  =set_min_max( PRED(129:256,129:256)  ,...
                                                {TRUE_4_min,TRUE_4_max})                           ;
        else
            if size(TRUE,1)==512
                [TRUE_1_min ,TRUE_1_max ]=get_min_max( TRUE(1  :128,1  :128))                      ;
                [TRUE_2_min ,TRUE_2_max ]=get_min_max( TRUE(129:256,1  :128))                      ;
                [TRUE_3_min ,TRUE_3_max ]=get_min_max( TRUE(257:384,1  :128))                      ;
                [TRUE_4_min ,TRUE_4_max ]=get_min_max( TRUE(385:512,1  :128))                      ;
                [TRUE_5_min ,TRUE_5_max ]=get_min_max( TRUE(1  :128,129:256))                      ;
                [TRUE_6_min ,TRUE_6_max ]=get_min_max( TRUE(129:256,129:256))                      ;
                [TRUE_7_min ,TRUE_7_max ]=get_min_max( TRUE(257:384,129:256))                      ;
                [TRUE_8_min ,TRUE_8_max ]=get_min_max( TRUE(385:512,129:256))                      ;
                [TRUE_9_min ,TRUE_9_max ]=get_min_max( TRUE(1  :128,257:384))                      ;
                [TRUE_10_min,TRUE_10_max]=get_min_max( TRUE(129:256,257:384))                      ;
                [TRUE_11_min,TRUE_11_max]=get_min_max( TRUE(257:384,257:384))                      ;
                [TRUE_12_min,TRUE_12_max]=get_min_max( TRUE(385:512,257:384))                      ;
                [TRUE_13_min,TRUE_13_max]=get_min_max( TRUE(1  :128,385:512))                      ;
                [TRUE_14_min,TRUE_14_max]=get_min_max( TRUE(129:256,385:512))                      ;
                [TRUE_15_min,TRUE_15_max]=get_min_max( TRUE(257:384,385:512))                      ;
                [TRUE_16_min,TRUE_16_max]=get_min_max( TRUE(385:512,385:512))                      ;                 
                PRED(1  :128,1  :128)    =set_min_max( PRED(1  :128,1  :128)  ,...
                                                      {TRUE_1_min ,TRUE_1_max})                    ;
                PRED(129:256,1  :128)    =set_min_max( PRED(129:256,1  :128)  ,...
                                                      {TRUE_2_min ,TRUE_2_max})                    ;
                PRED(257:384,1  :128)    =set_min_max( PRED(257:384,1  :128)  ,...
                                                      {TRUE_3_min ,TRUE_3_max})                    ;
                PRED(385:512,1  :128)    =set_min_max( PRED(385:512,1  :128)  ,...
                                                      {TRUE_4_min ,TRUE_4_max})                    ;
                PRED(1  :128,129:256)    =set_min_max( PRED(1  :128,129:256)  ,...
                                                      {TRUE_5_min ,TRUE_5_max})                    ;
                PRED(129:256,129:256)    =set_min_max( PRED(129:256,129:256)  ,...
                                                      {TRUE_6_min ,TRUE_6_max})                    ;
                PRED(257:384,129:256)    =set_min_max( PRED(257:384,129:256)  ,...
                                                      {TRUE_7_min ,TRUE_7_max})                    ;
                PRED(385:512,129:256)    =set_min_max( PRED(385:512,129:256)  ,...
                                                      {TRUE_8_min ,TRUE_8_max})                    ;
                PRED(1  :128,257:384)    =set_min_max( PRED(1  :128,257:384)  ,...
                                                      {TRUE_9_min ,TRUE_9_max})                    ;
                PRED(129:256,257:384)    =set_min_max( PRED(129:256,257:384)  ,...
                                                      {TRUE_10_min,TRUE_10_max})                   ;
                PRED(257:384,257:384)    =set_min_max( PRED(257:384,257:384)  ,...
                                                      {TRUE_11_min,TRUE_11_max})                   ;
                PRED(385:512,257:384)    =set_min_max( PRED(385:512,257:384)  ,...
                                                      {TRUE_12_min,TRUE_12_max})                   ;
                PRED(1  :128,385:512)    =set_min_max( PRED(1  :128,385:512)  ,...
                                                      {TRUE_13_min,TRUE_13_max})                   ;
                PRED(129:256,385:512)    =set_min_max( PRED(129:256,385:512)  ,...
                                                      {TRUE_14_min,TRUE_14_max})                   ;
                PRED(257:384,385:512)    =set_min_max( PRED(257:384,385:512)  ,...
                                                      {TRUE_15_min,TRUE_15_max})                   ;
                PRED(385:512,385:512)    =set_min_max( PRED(385:512,385:512)  ,...
                                                      {TRUE_16_min,TRUE_16_max})                   ;                 
            end
        end
    end
 
    function [y1_EX_pred,y1_EY_pred,y1_HZ_pred]=set_scales(FDTD,PD)
        y1_EX_fdtd                         =FDTD{1}                                                ;
        y1_EY_fdtd                         =FDTD{2}                                                ;
        y1_HZ_fdtd                         =FDTD{3}                                                ;
        y1_EX_pred                         =  PD{1}                                                ;
        y1_EY_pred                         =  PD{2}                                                ;
        y1_HZ_pred                         =  PD{3}                                                ;
        y1_EX_pred                         =apply_scales(y1_EX_fdtd,y1_EX_pred)                    ;
        y1_EY_pred                         =apply_scales(y1_EY_fdtd,y1_EY_pred)                    ;
        y1_HZ_pred                         =apply_scales(y1_HZ_fdtd,y1_HZ_pred)                    ;
    end

    function plot_config(DATA,NUMCOL,SUBPLOTNUM,LABEL)
        subplot(4,NUMCOL,SUBPLOTNUM)
        surf(DATA)
        axis    equal
        set(gca,'xticklabel',[],'yticklabel',[])
        shading interp
        xlabel(LABEL)
        view(2)        
    end    

    function plotter(DATA,FILENAME,TAG)
        close all        
        SIZE_SCALE   =1.25                                                                         ;
        h            =figure('Position',[520  378 SIZE_SCALE*560  SIZE_SCALE*420])                 ;
        sgt          =sgtitle({'University of New Mexico','Computational EM Lab',...
                               strcat('Time-Step :'," ",TAG)})                                     ;
        sgt.FontSize =10                                                                           ;        
        Y1_EX_       =DATA{1}                                                                      ;        
        Y1_EY_       =DATA{2}                                                                      ;
        Y1_HZ_       =DATA{3}                                                                      ;
        y1_fdtd_EX_  =DATA{4}                                                                      ;
        y1_fdtd_EY_  =DATA{5}                                                                      ;
        y1_fdtd_HZ_  =DATA{6}                                                                      ;
        y1_EX_       =DATA{7}                                                                      ;
        y1_EY_       =DATA{8}                                                                      ;
        y1_HZ_       =DATA{9}                                                                      ;                
        power_Y1     =0.5.*abs(Y1_HZ_)     .*sqrt(Y1_EX_     .^2+Y1_EY_     .^2)                   ;
        power_y1_fdtd=0.5.*abs(y1_fdtd_HZ_).*sqrt(y1_fdtd_EX_.^2+y1_fdtd_EY_.^2)                   ;
        power_y1     =0.5.*abs(y1_HZ_)     .*sqrt(y1_EX_     .^2+y1_EY_     .^2)                   ;
        plot_config(Y1_EX_  ,3,1 ,'Ex'                                         )
        plot_config(Y1_EY_  ,3,4 ,'Ey'                                         )
        plot_config(Y1_HZ_  ,3,7 ,'Hz'                                         )
        plot_config(power_Y1,3,10,{'Power',...
                                   'FDTD' ,...
                                   strcat('Step:'," ",TAG)}                    )        
        plot_config(y1_fdtd_EX_  ,3,2 ,'Ex'                                    )
        plot_config(y1_fdtd_EY_  ,3,5 ,'Ey'                                    )
        plot_config(y1_fdtd_HZ_  ,3,8 ,'Hz'                                    )
        plot_config(power_y1_fdtd,3,11,{'Power',...
                                   'FDTD' ,...
                                   strcat('Step:'," ",num2str(str2num(TAG)+1))})                               
        plot_config(y1_EX_  ,3,3 ,'Ex'                                         )
        plot_config(y1_EY_  ,3,6 ,'Ey'                                         )
        plot_config(y1_HZ_  ,3,9 ,'Hz'                                         )
        plot_config(power_y1,3,12,{'Power',...
                                   'EXPONENTIAL-DDM-FDTD' ,...
                                   strcat('Step:'," ",num2str(str2num(TAG)+1))})
        saveas(gcf,FILENAME,'png')
        close all
    end    

end

