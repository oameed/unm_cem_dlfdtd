# Deep Learning Finite Difference Time Domain (DLFDTD)

This is the official repository for the following work published in [_IEEE Open Journal of Antennas and Propagation (OJAP)_](https://www.ieeeaps.org/ieee-ojap):
* [2020 OJAP](https://ieeexplore.ieee.org/document/9158400) Noakoasteen. Wang. Peng. Christodoulou. _**"Physics-Informed Deep Neural Networks for Transient Electromagnetic Analysis"**_  

This work, in various stages of its development, has also been presented at the following conferences:  
* [2019 APS/URSI](https://www.2019apsursi.org/Papers/ViewPapers.asp?PaperNum=2120) Deep Neural Network Representations of Transient Electrodynamic Phenomena [\[Abstract from USNC-URSI Archive\]](https://usnc-ursi-archive.org/aps-ursi/2019/abstracts/2120.pdf)  
* [2019 NEMO](https://ieeexplore.ieee.org/document/8490271) Physics-Informed Deep Neural Networks for Transient Electromagnetic Analysis; _Ranked 3rd (Third) in Student Paper Competition_  
* [2018 APS/URSI](https://www.2018apsursi.org/Papers/ViewPapers.asp?PaperNum=2788) Emulation of Transient Electrodynamic Physics via Deep Neural Networks [\[Abstract from USNC-URSI Archive\]](https://usnc-ursi-archive.org/aps-ursi/2018/papers/2788.pdf)  

## Acknowledgements

* We would like to thank the University of New Mexico Center for Advanced Research Computing [(CARC)](http://carc.unm.edu/), supported in part by the National Science Foundation, for providing the high performance computing resources used in this work.

## References

_Similar Works_

[[1 ].](https://arxiv.org/abs/1903.00033              )&nbsp;&nbsp;2019. Compressed Convolutional LSTM: An Efficient Deep Learning framework to Model High Fidelity 3D Turbulence  
[[2 ].](https://arxiv.org/abs/1705.09036              )&nbsp;&nbsp;2017. Lat-Net: Compressing Lattice Boltzmann Flow Simulations using Deep Neural Networks  
[[3 ].](https://arxiv.org/abs/1607.03597              )&nbsp;&nbsp;2016. Accelerating Eulerian Fluid Simulation With Convolutional Networks  
[[4 ].](https://dl.acm.org/doi/10.1145/2939672.2939738)&nbsp;&nbsp;2016. Convolutional Neural Networks for Steady Flow Approximation  
[[5 ].](https://doi.org/10.1023/A:1023219016301       )&nbsp;&nbsp;1998. Preconditioning the Matrix Exponential Operator with Applications

_Deep Learning Algorithms and Architectures_

[[6 ].](https://www.deeplearningbook.org/)&nbsp;&nbsp;2016. Deep Learning  
[[7 ].](https://arxiv.org/abs/1609.02612 )&nbsp;&nbsp;2016. Generating Videos with Scene Dynamics  
[[8 ].](https://arxiv.org/abs/1512.03385 )&nbsp;&nbsp;2015. Deep Residual Learning for Image Recognition  
[[9 ].](https://arxiv.org/abs/1511.05440 )&nbsp;&nbsp;2015. Deep multi-scale video prediction beyond mean square error  
[[10].](https://arxiv.org/abs/1506.04214 )            2015. Convolutional LSTM Network: A Machine Learning Approach for Precipitation Nowcasting  
[[11].](https://arxiv.org/abs/1502.03167 )            2015. Batch Normalization: Accelerating Deep Network Training by Reducing Internal Covariate Shift  
[[12].](https://arxiv.org/abs/1502.01852 )            2015. Delving Deep into Rectifiers: Surpassing Human-Level Performance on ImageNet Classification  
[[13].](https://arxiv.org/abs/1409.1556  )            2014. Very Deep Convolutional Networks for Large-Scale Image Recognition  
[[14].](https://ieeexplore.ieee.org/document/5539957) 2010. Deconvolutional networks  

_Useful Web Tutorials_

[[15].](https://colah.github.io/posts/2015-08-Understanding-LSTMs/) 2015. Olah. Understanding LSTM Networks  
[[16].](https://www.quora.com/In-LSTM-how-do-you-figure-out-what-size-the-weights-are-supposed-to-be#:~:text=This%20is%20also%20called%20the,Size%20of%20the%20input%20batch.) 2017. Choudhary. In LSTM, how do you figure out what size the weights are supposed to be?  

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.74  T=0.18 s (287.0 files/s, 27359.4 lines/s)
--------------------------------------------------------------------------------
Language                      files          blank        comment           code
--------------------------------------------------------------------------------
MATLAB                           23            199            374           1988
Python                           13            127            273           1334
Markdown                          1             51              0            231
Bourne Shell                     10             73             18            107
YAML                              1              1              0             81
Bourne Again Shell                4             28             28             44
--------------------------------------------------------------------------------
SUM:                             52            479            693           3785
--------------------------------------------------------------------------------
</pre>

## How to Run

* This project uses [Tensorflow](https://www.tensorflow.org/) Version 1.14.0. The `YAML` file for creating the conda environment used to run this project is included in `run/conda`  
* Training network `v23`using dataset `type2` is used as an example of running an experiment in this project:
  1. On the Local Machine, Generate the Dataset:
     * `cd` to Main Project Directory
     * `./run/sh/generate_datasets_type_2.sh`
  2. On the HPC System   , Train the Network:
     * `cd` to `run/pbs`
     * `qsub train_v23.pbs`
  3. On the Local Machine, Generate Continuous Predictions and GIF's:
     * `cd` to Main Project Directory
     * `./run/sh/generate_predictions_v_23.sh`

## Dataset Types

|           |**Size**  |**Fields**                                          |**Objects**                                                      |
|:---:      |:---:     |:---                                                |:---                                                             |
**_Type1_** | 128x128  | Total Field / Scattered Field (TF/SF) Excitation   | PEC Objects (Circular, Rectangular or Both) at Random Locations |
**_Type2_** | 128x128  | Single Point Source Excitation at Random Locations | PEC Objects (Circular, Rectangular or Both) at Random Locations |
**_Type3_** | 128x128  | Two Point Source Excitations at Random Locations   | Single Circular PEC Object at a Single Location                 |
**_Type4_** | 256x256  | Single Point Source Excitation at Random Locations | Single Circular PEC Object at Random Locations                  |
**_Type5_** | 512x512  | Single Point Source Excitation at Single Location  | Single Circular PEC Object at a Single Location                 |

## Experiments v1x

**_Samples of 128x128 Training Examples_**

|     |     |     |
|:---:|:---:|:---:|
![alt text][fig_v1_1] | ![alt text][fig_v1_2] | ![alt text][fig_v1_3]
![alt text][fig_v1_4] | ![alt text][fig_v1_5] | ![alt text][fig_v1_6]

[fig_v1_1]:data/type1/gif/raw/simulation_24.gif
[fig_v1_4]:data/type1/gif/raw/simulation_30.gif
[fig_v1_2]:data/type1/gif/raw/simulation_40.gif
[fig_v1_5]:data/type1/gif/raw/simulation_46.gif
[fig_v1_3]:data/type1/gif/raw/simulation_23.gif
[fig_v1_6]:data/type1/gif/raw/simulation_22.gif

**_Samples of Testing and Continuous Predictions using 128x128 Testing Examples_**

| **_Testing_** | **_Continuous Prediction_** | **_Testing_**  | **_Continuous Prediction_** |
|:---:          |:---:                        |:---:           |:---:                        |
![alt text][fig_v1_7 ] | ![alt text][fig_v1_13] | ![alt text][fig_v1_8 ] | ![alt text][fig_v1_14]
![alt text][fig_v1_9 ] | ![alt text][fig_v1_15] | ![alt text][fig_v1_10] | ![alt text][fig_v1_16]
![alt text][fig_v1_11] | ![alt text][fig_v1_17] | ![alt text][fig_v1_12] | ![alt text][fig_v1_18]

[fig_v1_7 ]:networks/v13/videos/test/gif/simulation_13.gif  
[fig_v1_8 ]:networks/v13/videos/test/gif/simulation_43.gif  
[fig_v1_9 ]:networks/v13/videos/test/gif/simulation_31.gif  
[fig_v1_10]:networks/v13/videos/test/gif/simulation_51.gif  
[fig_v1_11]:networks/v13/videos/test/gif/simulation_20.gif  
[fig_v1_12]:networks/v13/videos/test/gif/simulation_47.gif  
[fig_v1_13]:networks/v13/videos/prediction/gif/simulation_13.gif  
[fig_v1_14]:networks/v13/videos/prediction/gif/simulation_43.gif  
[fig_v1_15]:networks/v13/videos/prediction/gif/simulation_31.gif  
[fig_v1_16]:networks/v13/videos/prediction/gif/simulation_51.gif  
[fig_v1_17]:networks/v13/videos/prediction/gif/simulation_20.gif  
[fig_v1_18]:networks/v13/videos/prediction/gif/simulation_47.gif  

## Experiments v2x

**_Samples of 128x128 Training Examples_**

|     |     |     |
|:---:|:---:|:---:|
![alt text][fig_v2_1] | ![alt text][fig_v2_2] | ![alt text][fig_v2_3]
![alt text][fig_v2_4] | ![alt text][fig_v2_5] | ![alt text][fig_v2_6]

[fig_v2_1]:data/type2/gif/raw/simulation_40.gif
[fig_v2_2]:data/type2/gif/raw/simulation_44.gif
[fig_v2_3]:data/type2/gif/raw/simulation_38.gif
[fig_v2_4]:data/type2/gif/raw/simulation_41.gif
[fig_v2_5]:data/type2/gif/raw/simulation_95.gif
[fig_v2_6]:data/type2/gif/raw/simulation_77.gif

**_Samples of Testing and Continuous Predictions using 128x128 Testing Examples_**

| **_Testing_** | **_Continuous Prediction_** | **_Testing_**  | **_Continuous Prediction_** |
|:---:          |:---:                        |:---:           |:---:                        |
![alt text][fig_v2_7 ] | ![alt text][fig_v2_13] | ![alt text][fig_v2_8 ] | ![alt text][fig_v2_14]
![alt text][fig_v2_9 ] | ![alt text][fig_v2_15] | ![alt text][fig_v2_10] | ![alt text][fig_v2_16]
![alt text][fig_v2_11] | ![alt text][fig_v2_17] | ![alt text][fig_v2_12] | ![alt text][fig_v2_18]

[fig_v2_7 ]:networks/v23/videos/test/gif/simulation_98.gif  
[fig_v2_8 ]:networks/v23/videos/test/gif/simulation_100.gif  
[fig_v2_9 ]:networks/v23/videos/test/gif/simulation_56.gif  
[fig_v2_10]:networks/v23/videos/test/gif/simulation_47.gif  
[fig_v2_11]:networks/v23/videos/test/gif/simulation_83.gif  
[fig_v2_12]:networks/v23/videos/test/gif/simulation_74.gif  
[fig_v2_13]:networks/v23/videos/prediction/gif/simulation_98.gif  
[fig_v2_14]:networks/v23/videos/prediction/gif/simulation_100.gif  
[fig_v2_15]:networks/v23/videos/prediction/gif/simulation_56.gif  
[fig_v2_16]:networks/v23/videos/prediction/gif/simulation_47.gif  
[fig_v2_17]:networks/v23/videos/prediction/gif/simulation_83.gif  
[fig_v2_18]:networks/v23/videos/prediction/gif/simulation_74.gif  

## Experiments v3x

**_Samples of 128x128 Training Examples_**

|     |     |     |
|:---:|:---:|:---:|
![alt text][fig_v3_1] | ![alt text][fig_v3_2] | ![alt text][fig_v3_3]
![alt text][fig_v3_4] | ![alt text][fig_v3_5] | ![alt text][fig_v3_6]

[fig_v3_1]:data/type3/gif/raw/simulation_2.gif
[fig_v3_2]:data/type3/gif/raw/simulation_4.gif
[fig_v3_3]:data/type3/gif/raw/simulation_5.gif
[fig_v3_4]:data/type3/gif/raw/simulation_6.gif
[fig_v3_5]:data/type3/gif/raw/simulation_43.gif
[fig_v3_6]:data/type3/gif/raw/simulation_47.gif

**_Samples of Testing and Continuous Predictions using 128x128 Testing Examples_**

| **_Testing_** | **_Continuous Prediction_** | **_Testing_**  | **_Continuous Prediction_** |
|:---:          |:---:                        |:---:           |:---:                        |
![alt text][fig_v3_7 ] | ![alt text][fig_v3_13] | ![alt text][fig_v3_8 ] | ![alt text][fig_v3_14]
![alt text][fig_v3_9 ] | ![alt text][fig_v3_15] | ![alt text][fig_v3_10] | ![alt text][fig_v3_16]
![alt text][fig_v3_11] | ![alt text][fig_v3_17] | ![alt text][fig_v3_12] | ![alt text][fig_v3_18]

[fig_v3_7 ]:networks/v33/videos/test/gif/simulation_3.gif  
[fig_v3_8 ]:networks/v33/videos/test/gif/simulation_8.gif  
[fig_v3_9 ]:networks/v33/videos/test/gif/simulation_34.gif  
[fig_v3_10]:networks/v33/videos/test/gif/simulation_36.gif  
[fig_v3_11]:networks/v33/videos/test/gif/simulation_38.gif  
[fig_v3_12]:networks/v33/videos/test/gif/simulation_39.gif  
[fig_v3_13]:networks/v33/videos/prediction/gif/simulation_3.gif  
[fig_v3_14]:networks/v33/videos/prediction/gif/simulation_8.gif  
[fig_v3_15]:networks/v33/videos/prediction/gif/simulation_34.gif  
[fig_v3_16]:networks/v33/videos/prediction/gif/simulation_36.gif  
[fig_v3_17]:networks/v33/videos/prediction/gif/simulation_38.gif  
[fig_v3_18]:networks/v33/videos/prediction/gif/simulation_39.gif  

## Experiments v4x

**_Samples of 256x256 Training Examples_**

|     |     |     |
|:---:|:---:|:---:|
![alt text][fig_v4_1] | ![alt text][fig_v4_2] | ![alt text][fig_v4_3]
![alt text][fig_v4_4] | ![alt text][fig_v4_5] | ![alt text][fig_v4_6]

[fig_v4_1]:data/type4/png/raw/simulation_1_190.png
[fig_v4_2]:data/type4/png/raw/simulation_2_235.png
[fig_v4_3]:data/type4/png/raw/simulation_5_245.png
[fig_v4_4]:data/type4/png/raw/simulation_3_230.png
[fig_v4_5]:data/type4/png/raw/simulation_4_240.png
[fig_v4_6]:data/type4/png/raw/simulation_6_250.png

**_Samples of Predictions using 256x256 Testing Examples_**

|     |     |     |     |
|:---:|:---:|:---:|:---:|
|**_True_**            |**_Prediction_**        |**_True_**              |**_Prediction_**        |
![alt text][fig_v4_7 ] | ![alt text][fig_v4_8 ] | ![alt text][fig_v4_9 ] | ![alt text][fig_v4_10] |
![alt text][fig_v4_11] | ![alt text][fig_v4_12] | ![alt text][fig_v4_13] | ![alt text][fig_v4_14] |
![alt text][fig_v4_15] | ![alt text][fig_v4_16] | ![alt text][fig_v4_17] | ![alt text][fig_v4_18] |
![alt text][fig_v4_19] | ![alt text][fig_v4_20] | ![alt text][fig_v4_21] | ![alt text][fig_v4_22] |

[fig_v4_7 ]:data/type4/png/raw/simulation_1_195.png
[fig_v4_9 ]:data/type4/png/raw/simulation_1_200.png
[fig_v4_11]:data/type4/png/raw/simulation_1_250.png
[fig_v4_13]:data/type4/png/raw/simulation_2_200.png
[fig_v4_15]:data/type4/png/raw/simulation_2_240.png
[fig_v4_17]:data/type4/png/raw/simulation_5_190.png
[fig_v4_19]:data/type4/png/raw/simulation_5_205.png
[fig_v4_21]:data/type4/png/raw/simulation_5_235.png
[fig_v4_8 ]:networks/v41/videos/expoddm/png/simulation_1_195.png
[fig_v4_10]:networks/v41/videos/expoddm/png/simulation_1_200.png
[fig_v4_12]:networks/v41/videos/expoddm/png/simulation_1_250.png
[fig_v4_14]:networks/v41/videos/expoddm/png/simulation_2_200.png
[fig_v4_16]:networks/v41/videos/expoddm/png/simulation_2_240.png
[fig_v4_18]:networks/v41/videos/expoddm/png/simulation_5_190.png
[fig_v4_20]:networks/v41/videos/expoddm/png/simulation_5_205.png
[fig_v4_22]:networks/v41/videos/expoddm/png/simulation_5_235.png

## Experiments v5x

**_Predictions for 512x512 Simulations Using Trained Network from Experiment v41_**

|     |     |     |     |
|:---:|:---:|:---:|:---:|
|**_True_**           |**_Prediction_**        |**_True_**              |**_Prediction_**        |
![alt text][fig_v5_1] | ![alt text][fig_v5_2 ] | ![alt text][fig_v5_3 ] | ![alt text][fig_v5_4 ] |
![alt text][fig_v5_5] | ![alt text][fig_v5_6 ] | ![alt text][fig_v5_7 ] | ![alt text][fig_v5_8 ] |
![alt text][fig_v5_9] | ![alt text][fig_v5_10] | ![alt text][fig_v5_11] | ![alt text][fig_v5_12] |
![alt text][fig_v5_13]| ![alt text][fig_v5_14] | ![alt text][fig_v5_15] | ![alt text][fig_v5_16] |
![alt text][fig_v5_17]| ![alt text][fig_v5_18] | ![alt text][fig_v5_19] | ![alt text][fig_v5_20] |
![alt text][fig_v5_21]| ![alt text][fig_v5_22] | ![alt text][fig_v5_23] | ![alt text][fig_v5_24] |
![alt text][fig_v5_25]| ![alt text][fig_v5_26] | ![alt text][fig_v5_27] | ![alt text][fig_v5_28] |


[fig_v5_1 ]:data/type5/png/raw/simulation_1_320.png
[fig_v5_3 ]:data/type5/png/raw/simulation_1_330.png
[fig_v5_5 ]:data/type5/png/raw/simulation_1_340.png
[fig_v5_7 ]:data/type5/png/raw/simulation_1_350.png
[fig_v5_9 ]:data/type5/png/raw/simulation_1_360.png
[fig_v5_11]:data/type5/png/raw/simulation_1_370.png
[fig_v5_13]:data/type5/png/raw/simulation_1_380.png
[fig_v5_15]:data/type5/png/raw/simulation_1_390.png
[fig_v5_17]:data/type5/png/raw/simulation_1_400.png
[fig_v5_19]:data/type5/png/raw/simulation_1_410.png
[fig_v5_21]:data/type5/png/raw/simulation_1_420.png
[fig_v5_23]:data/type5/png/raw/simulation_1_430.png
[fig_v5_25]:data/type5/png/raw/simulation_1_440.png
[fig_v5_27]:data/type5/png/raw/simulation_1_450.png

[fig_v5_2 ]:networks/v51/videos/expoddm/png/simulation_1_320.png
[fig_v5_4 ]:networks/v51/videos/expoddm/png/simulation_1_330.png
[fig_v5_6 ]:networks/v51/videos/expoddm/png/simulation_1_340.png
[fig_v5_8 ]:networks/v51/videos/expoddm/png/simulation_1_350.png
[fig_v5_10]:networks/v51/videos/expoddm/png/simulation_1_360.png
[fig_v5_12]:networks/v51/videos/expoddm/png/simulation_1_370.png
[fig_v5_14]:networks/v51/videos/expoddm/png/simulation_1_380.png
[fig_v5_16]:networks/v51/videos/expoddm/png/simulation_1_390.png
[fig_v5_18]:networks/v51/videos/expoddm/png/simulation_1_400.png
[fig_v5_20]:networks/v51/videos/expoddm/png/simulation_1_410.png
[fig_v5_22]:networks/v51/videos/expoddm/png/simulation_1_420.png
[fig_v5_24]:networks/v51/videos/expoddm/png/simulation_1_430.png
[fig_v5_26]:networks/v51/videos/expoddm/png/simulation_1_440.png
[fig_v5_28]:networks/v51/videos/expoddm/png/simulation_1_450.png

