############################################
### UNIVERSITY OF NEW MEXICO             ###
### COMPUTATIONAL EM LAB                 ###
### DEEP LEARNING PROJECT                ###
### DATA PROCESSING FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN               ###
############################################

import os
import numpy as np
from utilsd import getFILENAMES,sortFILENAMES,wFILE,rHDF

def genRNDlist(READPATH,SAVEPATH,DIV,EXPODDM=False):
 filenames=getFILENAMES(READPATH,False)
 np.random.shuffle(filenames) 
 trainlist=filenames[0:int(np.floor(DIV*len(filenames))) ]
 testlist =filenames[  int(np.floor(DIV*len(filenames))):]
 trainlist=sortFILENAMES(trainlist,EXPODDM)
 testlist =sortFILENAMES(testlist ,EXPODDM)
 wFILE(SAVEPATH,'listTRAIN',trainlist)
 wFILE(SAVEPATH,'listTEST' ,testlist )

def trim(DATA,SHAPE):
 difw=DATA.shape[1]-SHAPE[0]
 difh=DATA.shape[2]-SHAPE[1]
 if np.mod(difw,2)==0:
  skipwb=int(difw/2            )
  skipwe=int(difw/2            )
 else:
  skipwb=int(np.floor(difw/2)  )
  skipwe=int(np.floor(difw/2)+1)
 if np.mod(difh,2)==0:
  skiphb=int(difh/2            )
  skiphe=int(difh/2            )
 else:
  skiphb=int(np.floor(difh/2)  )
  skiphe=int(np.floor(difh/2)+1)
 DATA=DATA[:,skipwb:DATA.shape[1]-skipwe,skiphb:DATA.shape[2]-skiphe]
 return DATA

def getSCALE(HDFDATAPATH,FRAME,SHAPE):
 eta      =np.sqrt((4*np.pi*1e-7)/(8.85e-12)) 
 mag_max_E=np.array([])                                                                  # FIND THE MAX MAGNITUDE OF E-FIELD VECTOR / H-FIELD VECTOR 
 mag_max_H=np.array([])
 filenames=getFILENAMES(HDFDATAPATH)
 for i in range(len(filenames)):
  fobj,keys=rHDF(filenames[i])
  imgex       =trim(fobj['Ex_FIELD'][FRAME[0]-1:FRAME[1]-1], [SHAPE[0],SHAPE[1]]) 
  imgey       =trim(fobj['Ey_FIELD'][FRAME[0]-1:FRAME[1]-1], [SHAPE[0],SHAPE[1]])
  imghz       =trim(fobj['Hz_FIELD'][FRAME[0]-1:FRAME[1]-1], [SHAPE[0],SHAPE[1]])*eta
  maximum_E   =np.amax(np.sqrt(np.power(imgex,2)+np.power(imgey,2)))
  maximum_H   =np.amax(np.absolute(imghz))   
  mag_max_E   =np.append(mag_max_E,maximum_E)
  mag_max_H   =np.append(mag_max_H,maximum_H)
 scalefactor_E=np.amax(mag_max_E)
 scalefactor_H=np.amax(mag_max_H)
 return scalefactor_E,scalefactor_H

def normalize(EX,EY,HZ,SCALEFACTOR):
 for i in range(EX.shape[0]):
  EX[i]   =EX[i]/SCALEFACTOR
  EY[i]   =EY[i]/SCALEFACTOR
  HZ[i]   =HZ[i]/SCALEFACTOR
 maximum_e=np.amax(np.sqrt(np.power(EX,2)+np.power(EY,2)))
 maximum_h=np.amax(np.absolute(HZ))
 return EX,EY,HZ,maximum_e,maximum_h

def getFIELDS(FILENAME,FRAME,SHAPE,SCALEFACTOR):
 eta=np.sqrt((4*np.pi*1e-7)/(8.85e-12))
 # FIELD DATA: SELECT EVENTFUL TIME BLOCK
 # FIELD DATA: TRIM TO REQUIRED PROJECT DIMENSIONS
 # FIELD DATA: SCALE Hz COMPONENT WITH ETA (FREE-SPACE IMPEDANCE)
 # FIELD DATA: NORMALIZE EACH TIME-FRAME WITH SCALEFACTOR
 # FIELD DATA: STACK ALL FIELD COMPONENTS TO CREATE UNIFIED DATA STRUCTURE
 # FIELD DATA: SET THE DATA TYPE TO 'float32'
 fobj,keys=rHDF(FILENAME)
 imgex=trim(fobj['Ex_FIELD'][FRAME[0]-1:FRAME[1]-1], [SHAPE[0],SHAPE[1]]) 
 imgey=trim(fobj['Ey_FIELD'][FRAME[0]-1:FRAME[1]-1], [SHAPE[0],SHAPE[1]])
 imghz=trim(fobj['Hz_FIELD'][FRAME[0]-1:FRAME[1]-1], [SHAPE[0],SHAPE[1]])*eta
 imgex,imgey,imghz,maximum_e,maximum_h=normalize(imgex,imgey,imghz,SCALEFACTOR)
 img  =np.stack((imgex,imgey,imghz),axis=3).astype('float32')
 return img,maximum_e,maximum_h

def getBOUND(FILENAME,FRAME,SHAPE):
 # BOUNDARY DATA: CONVERT TO BINARY ('1':PEC,'0':FREE-SPACE)
 # BOUNDARY DATA: MAKE AN ARRAY OUT OF BOUNDARY THE SAME NUMBER OF TIME-FRAMES AS FIELDS
 # BOUNDARY DATA: TRIM TO REQUIRED PROJECT DIMENSIONS
 # BOUNDARY DATA: RESHAPE TO CONFORM TO HAVE A '1 CHANNEL' FORMAT
 # BOUNDARY DATA: SET THE DATA TYPE TO 'float32'
 fobj,keys=rHDF(FILENAME)
 shape    =fobj['boundary'].shape
 bnd      =np.zeros((shape[0],shape[1]))
 for  indx1 in range(shape[0]):
  for indx2 in range(shape[1]):
   if not fobj['boundary'][indx1,indx2]==0:
    bnd[indx1,indx2]=1
 bnd      =np.stack([ bnd for _ in range(FRAME[1]-FRAME[0])])
 bnd      =trim(bnd,[SHAPE[0],SHAPE[1]])
 bnd      =np.expand_dims(bnd,axis=-1).astype('float32')
 return bnd

##################################################
### EXPONENTIAL DOMAIN DECOMPOSITION FUNCTIONS ###
##################################################

def get_SUBDOM_from_VEC(VEC,NUM):
 SUBDOMAINLENGTH=int(VEC.shape[0]/NUM)
 SUBDOMAINS     =[]
 _SUBDOMAINS    =[]
 list_4_subdoms =[0,2,1,3]
 list_16_subdoms=[0,4,8,12,1,5,9,13,2,6,10,14,3,7,11,15] 
 for subdom in range(NUM):
  SUBDOMAINVEC=VEC[subdom*SUBDOMAINLENGTH:subdom*SUBDOMAINLENGTH+SUBDOMAINLENGTH]
  SIZEVEC     =int(np.sqrt(SUBDOMAINVEC.shape[0]/5))
  ex          =[]
  ey          =[]
  hz          =[]
  hzx         =[]
  hzy         =[]
  for k in range(SIZEVEC*SIZEVEC):
   ex.append (SUBDOMAINVEC[k*5+0])
   ey.append (SUBDOMAINVEC[k*5+1])
   hz.append (SUBDOMAINVEC[k*5+2])
   hzx.append(SUBDOMAINVEC[k*5+3])
   hzy.append(SUBDOMAINVEC[k*5+4])
  ex =np.reshape(np.array(ex ),[SIZEVEC,SIZEVEC])
  ey =np.reshape(np.array(ey ),[SIZEVEC,SIZEVEC])
  hz =np.reshape(np.array(hz ),[SIZEVEC,SIZEVEC])
  hzx=np.reshape(np.array(hzx),[SIZEVEC,SIZEVEC])
  hzy=np.reshape(np.array(hzy),[SIZEVEC,SIZEVEC])
  SUBDOMAINS.append(np.stack((ex,ey,hz,hzx,hzy),axis=2))  
 if NUM==4:
  for i in range(NUM):
   _SUBDOMAINS.append(SUBDOMAINS[list_4_subdoms[i]])   
 else:
  if NUM==16:
   for i in range(NUM):
    _SUBDOMAINS.append(SUBDOMAINS[list_16_subdoms[i]])
 _SUBDOMAINS=np.array(_SUBDOMAINS) 
 return _SUBDOMAINS

def get_SUBDOM_from_FULL(DATA,GLBNUM,NUM):
 _subdoms=[]
 for  i in range(DATA.shape[0]):
  for j in range(DATA.shape[1]):
   if not DATA[i,j]==0:
    DATA[i,j]=1
 for  i in range(NUM            ):
  _subdoms_temp=[]
  for j in range(GLBNUM.shape[1]):
   if GLBNUM[1,j]==(i+1):
    _subdoms_temp.append(DATA[int(GLBNUM[2,j]-1),int(GLBNUM[3,j]-1)])
  _subdoms.append(np.reshape(np.array(_subdoms_temp),[128,128]))
 SUBDOMAINS=np.array(_subdoms) 
 return SUBDOMAINS

def get_num_subdoms(LIST):
 LIST    =np.transpose(LIST,[1,0])
 sub_doms=[]
 for i in range(LIST.shape[0]):
  if i==0:
   sub_doms.append(LIST[i,1])
  else:
   if not LIST[i,1] in sub_doms:
    sub_doms.append(LIST[i,1]) 
 return len(sub_doms)

def rEXPODDMDATABND(FILENAME):  
 fobj,keys =rHDF(FILENAME)
 glbnum    =fobj['glbnum']
 bnd       =fobj['MPex'  ][1]
 num_subdom=get_num_subdoms(glbnum) 
 bnd       =get_SUBDOM_from_FULL(bnd,glbnum,num_subdom)
 bnd       =np.transpose        (bnd,[0,2,1]          ) 
 return bnd

def rEXPODDMDATAFIELD(FILENAME):
 fobj,keys =rHDF(FILENAME) 
 glbnum    =fobj['glbnum']
 Y1_fdtd   =fobj['fdtd'  ][0]
 num_subdom=get_num_subdoms(glbnum) 
 Y1_fdtd   =get_SUBDOM_from_VEC (Y1_fdtd,num_subdom)
 w         =fobj['w'     ][0]
 w         =get_SUBDOM_from_VEC (w      ,num_subdom)
 return Y1_fdtd,w

def getEXPODDMSCALE(FILENAMES):
 eta           =np.sqrt((4*np.pi*1e-7)/(8.85e-12))
 scaleE_Y1_fdtd=[]
 scaleH_Y1_fdtd=[]
 scaleE_w      =[]
 scaleH_w      =[]
 for i in range(len(FILENAMES)):
  Y1_fdtd,w    =rEXPODDMDATAFIELD(FILENAMES[i])
  mag_E_Y1_fdtd=np.sqrt(np.power(Y1_fdtd[:,:,:,0],2)+np.power(Y1_fdtd[:,:,:,1],2))
  mag_H_Y1_fdtd=np.absolute(Y1_fdtd[:,:,:,2]*eta)
  mag_E_w      =np.sqrt(np.power(w      [:,:,:,0],2)+np.power(w      [:,:,:,1],2))
  mag_H_w      =np.absolute(w      [:,:,:,2]*eta)
  scaleE_Y1_fdtd.append(np.amax(mag_E_Y1_fdtd))
  scaleH_Y1_fdtd.append(np.amax(mag_H_Y1_fdtd))
  scaleE_w.append      (np.amax(mag_E_w      ))
  scaleH_w.append      (np.amax(mag_H_w      )) 
 return [max(scaleE_Y1_fdtd),max(scaleH_Y1_fdtd),max(scaleE_w),max(scaleH_w)]

def getEXPODDMDATA(FILENAME,SCALES): 
 def get_max_mag_E_H(DATA):
  max_mag_e=np.amax(np.sqrt(np.power(DATA[:,:,:,0],2)+np.power(DATA[:,:,:,1],2)))
  max_mag_h=np.amax(np.absolute(DATA[:,:,:,2]))
  return max_mag_e,max_mag_h
 eta               =np.sqrt((4*np.pi*1e-7)/(8.85e-12))
 Y1_fdtd,w         =rEXPODDMDATAFIELD(FILENAME)
 bnd               =rEXPODDMDATABND  (FILENAME)
 Y1_fdtd[:,:,:,2:5]=Y1_fdtd[:,:,:,2:5]*eta                                               # BRING H-FIELD TO THE SAME LEVEL AS E-FIELD
 Y1_fdtd           =Y1_fdtd/SCALES[0]
 w      [:,:,:,2:5]=w      [:,:,:,2:5]*eta                                               # BRING H-FIELD TO THE SAME LEVEL AS E-FIELD
 w                 =w      /SCALES[1]
 Y1_fdtd_max_mag_e,Y1_fdtd_max_mag_h=get_max_mag_E_H(Y1_fdtd)
 w_max_mag_e      ,w_max_mag_h      =get_max_mag_E_H(w      )
 return Y1_fdtd,w,bnd,[Y1_fdtd_max_mag_e,Y1_fdtd_max_mag_h],[w_max_mag_e,w_max_mag_h]


