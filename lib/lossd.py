#################################
### UNIVERSITY OF NEW MEXICO  ###
### COMPUTATIONAL EM LAB      ###
### DEEP LEARNING PROJECT     ###
### LOSS FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN    ###
#################################

import numpy      as np
import tensorflow as tf
from paramd import GDLFACTOR

def FROBENIUS_NORM(YPRED,YTRUE):
 with tf.name_scope('FROBENIUS_NORM'):
  x=tf.subtract(YPRED,YTRUE)
  x=tf.square(x)
  x=tf.reduce_sum(x,axis=[1,2,3,4])
  x=tf.multiply(x,0.5)
  x=tf.sqrt(x)
  x=tf.reduce_mean(x)
 return x

def GDL(YPRED,YTRUE):
 def GRAD(X):
  x=tf.square(X)
  x=tf.reduce_sum(x,axis=[1,2,3,4])
  x=tf.multiply(x,0.5)
  x=tf.reduce_mean(x)
  return x
 shape                =YTRUE.get_shape().as_list() 
 YTRUE_x_shifted_right=tf.slice(YTRUE,[0,0,1,0,0],[shape[0],shape[1],shape[2]-1,shape[3]  ,shape[4]])
 YTRUE_x_shifted_left =tf.slice(YTRUE,[0,0,0,0,0],[shape[0],shape[1],shape[2]-1,shape[3]  ,shape[4]])
 YTRUE_x_GRAD         =tf.abs  (YTRUE_x_shifted_right-YTRUE_x_shifted_left                          )
 YPRED_x_shifted_right=tf.slice(YPRED,[0,0,1,0,0],[shape[0],shape[1],shape[2]-1,shape[3]  ,shape[4]])
 YPRED_x_shifted_left =tf.slice(YPRED,[0,0,0,0,0],[shape[0],shape[1],shape[2]-1,shape[3]  ,shape[4]])
 YPRED_x_GRAD         =tf.abs  (YPRED_x_shifted_right-YPRED_x_shifted_left                          )
 LOSS_x_GRAD          =GRAD    (YPRED_x_GRAD-YTRUE_x_GRAD                                           )
 YTURE_y_shifted_right=tf.slice(YTRUE,[0,0,0,1,0],[shape[0],shape[1],shape[2]  ,shape[3]-1,shape[4]])
 YTURE_y_shifted_left =tf.slice(YTRUE,[0,0,0,0,0],[shape[0],shape[1],shape[2]  ,shape[3]-1,shape[4]])
 YTRUE_y_GRAD         =tf.abs  (YTURE_y_shifted_right-YTURE_y_shifted_left                          )
 YPRED_y_shifted_right=tf.slice(YPRED,[0,0,0,1,0],[shape[0],shape[1],shape[2]  ,shape[3]-1,shape[4]])
 YPRED_y_shifted_left =tf.slice(YPRED,[0,0,0,0,0],[shape[0],shape[1],shape[2]  ,shape[3]-1,shape[4]])
 YPRED_y_GRAD         =tf.abs  (YPRED_y_shifted_right-YPRED_y_shifted_left                          )
 LOSS_y_GRAD          =GRAD    (YPRED_y_GRAD-YTRUE_y_GRAD                                           )
 LOSS_GDL             =LOSS_x_GRAD+LOSS_y_GRAD
 return LOSS_GDL

def loss(YPRED,YTRUE,MODE='train',TYPE='fdtd'): 
 with tf.name_scope('LOSS'+'_'+MODE):
  if TYPE=='fdtd':
   x     =FROBENIUS_NORM(YPRED,YTRUE)+GDLFACTOR*GDL(YPRED,YTRUE)
  else:
   if TYPE=='ddm':    
    YTRUE=YTRUE[:,None,1,:,:,:]
    YPRED=YPRED[:,None,1,:,:,:]        
    x    =FROBENIUS_NORM(YPRED,YTRUE)+GDLFACTOR*GDL(YPRED,YTRUE)  
  tf.compat.v1.add_to_collection(tf.compat.v1.GraphKeys.ACTIVATIONS,x)                                  # SUMMARY OPS
 return x


