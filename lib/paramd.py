################################
### UNIVERSITY OF NEW MEXICO ###
### COMPUTATIONAL EM LAB     ###
### DEEP LEARNING PROJECT    ###
### PARAMETER DEFINITIONS    ###
### by: OAMEED NOAKOASTEEN   ###
################################

import os
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-data'      , type=str  ,               help='NAME-OF-DATA-TYPE-DIRECTORY'       ,required=True         )
parser.add_argument('-net'       , type=str  , default='v00',help='NAME-OF-NETWORK-PROJECT'                                  )
  # GROUP: HYPER-PARAMETERS FOR PRE-PROCESSING OF DATA
parser.add_argument('-simb'      , type=int  , default=190  ,help='SIMULATION-BEGINNING-FRAME'                               )
parser.add_argument('-sime'      , type=int  , default=400  ,help='SIMULATION-END-FRAME'                                     )
parser.add_argument('-W'         , type=int  , default=128  ,help='WIDTH-OF-IMAGE'                                           )
parser.add_argument('-H'         , type=int  , default=128  ,help='HEIGHT-OF-IMAGE'                                          )
parser.add_argument('-list'      ,                           help='GENERATE-RANDOM-TRAIN-TEST-LISTS'  ,action  ='store_true' )
parser.add_argument('-div'       , type=float, default=0.75 ,help='PORTION-OF-TOTAL-ASSIGNED-TO-TRAIN'                       )
  # GROUP: HYPER-PARAMETERS FOR TRAINING AND TESTING
parser.add_argument('-b'         , type=int  , default=4    ,help='BATCH-SIZE'                                               )
parser.add_argument('-t'         , type=int  , default=5    ,help='VIDEO-SIZE'                                               )
parser.add_argument('-c'         , type=int  , default=3    ,help='NUMBER-OF-CHANNELS'                                       )
parser.add_argument('-eptr'      , type=int  , default=250  ,help='NUMBER-OF-TRAINING-EPOCHS'                                )
parser.add_argument('-steptr'    , type=int  , default=50   ,help='EVERY-TRAIN-STEPS-TO-WRITE-SUMMERIES'                     )
parser.add_argument('-epts'      , type=int  , default=1    ,help='NUMBER-OF-TESTING-EPOCHS'                                 )
parser.add_argument('-stepts'    , type=int  , default=1    ,help='EVERY-TEST-STEPS-TO-WRITE-SUMMERIES'                      )
parser.add_argument('-bc'        , type=int  , default=10000,help='BATCH-BUFFER-CAPACITY'                                    )
parser.add_argument('-lr'        , type=float, default=0.001,help='LEARNING-RATE'                                            )
parser.add_argument('-ckptn'     , type=int  , default=1    ,help='NUMBER-OF-CHECKPOINTS-TO-KEEP'                            )
parser.add_argument('-gdl'       , type=float, default=0.004,help='GDL-LOSS-COEFFICIENT'                                     )
parser.add_argument('-nointscale',                           help='NO INTERNAL SCALING IN NETWORK'    ,action  ='store_true' )

### ENABLE FLAGS
args=parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES
PATHS    =[os.path.join('..','..','data'    ,args.data,'hdf5'       ,'raw'             ),
           os.path.join('..','..','data'    ,args.data,'hdf5'       ,'processed'       ),           
           os.path.join('..','..','data'    ,args.data,'info'                          ),
           os.path.join('..','..','networks',args.net ,'model'                         ),
           os.path.join('..','..','networks',args.net ,'tfrecords'                     ),
           os.path.join('..','..','networks',args.net ,'checkpoints',                  ),
           os.path.join('..','..','networks',args.net ,'logs'                          ),
           os.path.join('..','..','networks',args.net ,'videos'     ,'test'      ,'hdf'),
           os.path.join('..','..','networks',args.net ,'videos'     ,'prediction','hdf')]           

FRAMEMNGM=[args.simb,args.sime ,args.W     ,args.H                            ]
genLIST  = args.list
trainDIV = args.div
NETV     = args.net
numT     = args.t
numC     = args.c
BATCH    =[args.b   ,args.bc                                                  ]
OPTIM    =[args.lr  ,args.eptr ,args.steptr,args.ckptn,args.epts,args.stepts  ]
GDLFACTOR= args.gdl
VIDFRAME =[args.t   ,args.simb ,args.sime                                     ]
INTSCALE = args.nointscale

# PATHS[0]    : HDFRAWPATH   
# PATHS[1]    : HDFPRCPATH   
# PATHS[2]    : INFOPATH     
# PATHS[3]    : MODELPATH    
# PATHS[4]    : TFRPATH      
# PATHS[5]    : CKPTPATH     
# PATHS[6]    : LOGPATH      
# PATHS[7]    : VIDTESTPATH  
# PATHS[8]    : VIDPREDPATH  
# FRAMEMNGM[0]: BEGINNING INDEX OF THE EVENTFUL PORTION OF THE SIMULATION 
# FRAMEMNGM[1]: ENDING    INDEX OF THE EVENTFUL PORTION OF THE SIMULATION 
# FRAMEMNGM[2]: WIDTH  OF EACH FRAME
# FRAMEMNGM[3]: HEIGHT OF EACH FRAME
# genLIST     : GENERATE TRAIN/TEST LISTS
# trainDIV    : TRAIN/TEST DIVITION: NUMBER OF TRAIN SAMPLES ARE DIV*TOTAL
# NETV        : NETWORK VERSION
# numT        : NUMBER OF TIME-FRAMES (VIDEO SIZE)
# numC        : NUMBER OF INPUT CHANNELS
# BATCH[0]    : BATCH SIZE
# BATCH[1]    : BATCH CAPACITY
# OPTIM[0]    : LEARNING RATE
# OPTIM[1]    : NUMBER OF EPOCHS FOR TRAINING
# OPTIM[2]    : STEPS: EVERY 'STEPS' WRITE SUMMARY/CHECKPOINT FOR TRAINING
# OPTIM[3]    : MAXIMUM NUMBER OF CHECKPOINTS TO KEEP
# OPTIM[4]    : NUMBER OF EPOCHS FOR TESTING
# OPTIM[5]    : STEPS: EVERY 'STEPS' WRITE SUMMARY/CHECKPOINT FOR TESTING
# GDLFACTOR   : GDL LOSS COEFFICIENT
# VIDFRAME    : VIDEO FRAME MANAGEMENT FOR VIDEO GENERATION


