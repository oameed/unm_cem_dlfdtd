###########################################
### UNIVERSITY OF NEW MEXICO            ###
### COMPUTATIONAL EM LAB                ###
### DEEP LEARNING PROJECT               ###
### INPUT PIPELINE FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN              ###
###########################################

import os
import numpy      as np
import tensorflow as tf
from utilsd import getFILENAMES,sortFILENAMES,rHDF,rFILE,write_summaries_image

def fetchSHAPE(HDFPATH,FILEINDEX=0):
 filenames=getFILENAMES (HDFPATH  ,False             )
 filenames=sortFILENAMES(filenames,False             )
 filename =os.path.join (HDFPATH,filenames[FILEINDEX])
 fobj,keys=rHDF         (filename                    )
 shape    =fobj['img'].shape
 shape    =[shape[0],shape[1],shape[2]]
 return shape

def getCHANNELS(DATA,CN):
 # THIS FUNCTION RETURNS APPROPRIATE FIELD COMPONENTS FOR THE GIVEN NUMBER OF CHANNELS
 # THE ORDER OF FIELD DATA IN CHANNELS IS Ex,Ey,Hz
 # FOR CN==1 ONLY Hz IS RETURNED
 # FOR CN==2 Ex AND Ey ARE RETURNED
 if CN==1:
  data=DATA[:,:,:,2]
  data=np.expand_dims(data,axis=-1)
 else:
  if CN==2:
   data=DATA[:,:,:,0:2]
  else:
   if CN==3:
    data=DATA
 return data

def saveTFRECORDS(HDFPATH,SHAPE,WRITEPATH,LIST): 
 filenames =[]                                                                                          # GET FULL FILE NAMES
 _filenames=rFILE(LIST)
 for i in range(len(_filenames)):
  filenames.append(os.path.join(HDFPATH,_filenames[i])) 
 for i in range(len(filenames)):                                                                        # WRITE TO TFRECORDS 
  fobj,keys   =rHDF(filenames[i])  
  img         =getCHANNELS(fobj['img'],SHAPE[1])                                                        # LOAD 'numC' CHANNELS FROM FIELD DATA
  bnd         =            fobj['bnd']  
  shape       =img.shape
  img         =np.reshape(img,[int(np.floor(shape[0]/SHAPE[0])),SHAPE[0],shape[1],shape[2],shape[3]])   # RESHAPE TO VIDEO FORMAT
  bnd         =np.reshape(bnd,[int(np.floor(shape[0]/SHAPE[0])),SHAPE[0],shape[1],shape[2],1       ])  
  savefilename=os.path.join(WRITEPATH,filenames[i].split('/')[6].split('.')[0]+'.tfrecords')            # GET TFRECORDS FILE NAME  
  print(' WRITING ',filenames[i].split('/')[6],' TO TFRECORDS FILE ')                                   # WRITE EACH EXAMPLE TO TFRECORDS FILE
  writer      =tf.io.TFRecordWriter(savefilename)
  for index in range(img.shape[0]):
   img_example=np.reshape(img[index],np.prod(img[index].shape))
   bnd_example=np.reshape(bnd[index],np.prod(bnd[index].shape))
   feat={'images'  :  tf.train.Feature(bytes_list=tf.train.BytesList(value  =[img_example.tostring()])),
         'boundary':  tf.train.Feature(bytes_list=tf.train.BytesList(value  =[bnd_example.tostring()]))}  
   example    =       tf.train.Example(features  =tf.train.Features (feature=feat))
   writer.write(example.SerializeToString())
  writer.close() 

def inputTFRECORDS(DATAPATH,SHAPE,POPTIM,PBATCH,MODE): 
 if MODE=='train':                                                                                      # SET NUMBER OF EPOCHS
  epoch_num=POPTIM[1]
 else:
  if MODE=='test':
   epoch_num=POPTIM[4] 
 def parse_fn(SERIALIZED_EXAMPLE):                                                                      # DEFINE PARSING FUNCTION
  FEAT    ={'images'  : tf.io.FixedLenFeature([],tf.string),
            'boundary': tf.io.FixedLenFeature([],tf.string)}
  FEATURES=tf.io.parse_single_example(SERIALIZED_EXAMPLE,features=FEAT)
  _vid    =tf.decode_raw(FEATURES['images'  ],tf.float32)
  _bnd    =tf.decode_raw(FEATURES['boundary'],tf.float32)  
  _vid.set_shape([np.prod(SHAPE)              ])
  _bnd.set_shape([SHAPE[0]*SHAPE[1]*SHAPE[2]*1])
  vid     =tf.reshape(_vid,SHAPE)
  bnd     =tf.reshape(_bnd,[SHAPE[0],SHAPE[1],SHAPE[2],1])
  return vid,bnd 
 filenames=getFILENAMES(DATAPATH)                                                                       # COLLECT FILE NAMES 
 with tf.name_scope('READ'+'_'+'TFRecords'+'_'+MODE):                                                   # BEGIN READING DATA
  dataset      =tf.data.TFRecordDataset(filenames)
  dataset      =dataset.map(parse_fn)
  dataset      =dataset.shuffle(PBATCH[1])
  dataset      =dataset.repeat(epoch_num)
  dataset      =dataset.batch(PBATCH[0],drop_remainder=True)  
  iterator     =tf.compat.v1.data.make_one_shot_iterator(dataset)                                       # INITIALIZE ONE-SHOT ITERATOR
  next_element =iterator.get_next()
  vid          =next_element[0]
  bnd          =next_element[1]  
 return vid,bnd

def TESTVIDSTFRECORDS(FILENAME,SHAPE,BATCH): 
 def parse_fn(SERIALIZED_EXAMPLE):                                                                      # DEFINE PARSING FUNCTION
  FEAT    ={'images'  : tf.io.FixedLenFeature([],tf.string),
            'boundary': tf.io.FixedLenFeature([],tf.string)}
  FEATURES=tf.io.parse_single_example(SERIALIZED_EXAMPLE,features=FEAT)
  _vid=tf.decode_raw(FEATURES['images'  ],tf.float32)
  _bnd=tf.decode_raw(FEATURES['boundary'],tf.float32)
  _vid.set_shape([np.prod(SHAPE)])
  _bnd.set_shape([SHAPE[0]*SHAPE[1]*SHAPE[2]*1])
  vid=tf.reshape(_vid,SHAPE)
  bnd=tf.reshape(_bnd,[SHAPE[0],SHAPE[1],SHAPE[2],1])
  return vid,bnd
 dataset      =tf.data.TFRecordDataset(FILENAME)                                                        # BEGIN READING DATA
 dataset      =dataset.map(parse_fn)
 dataset      =dataset.batch(BATCH,drop_remainder=True)
 iterator     =tf.compat.v1.data.make_one_shot_iterator(dataset)                                        # INITIALIZE ONE-SHOT ITERATOR
 next_element =iterator.get_next()
 vid          =next_element[0]
 bnd          =next_element[1]
 return vid,bnd

##################################################
### EXPONENTIAL DOMAIN DECOMPOSITION FUNCTIONS ###
##################################################

def fetchEXPODDMSHAPE(HDFPATH,FILEINDEX=0):
 filenames=getFILENAMES(HDFPATH,False) 
 filename =os.path.join(HDFPATH,filenames[FILEINDEX]) 
 fobj,keys=rHDF(filename)
 shape    =fobj['fdtd'].shape
 shape    =[shape[0],shape[1],shape[2],shape[3]]
 return shape

def getEXPODDMDATASETS(FILENAME,TYPE,TIME=[]): 
 fobj,keys =rHDF(FILENAME)
 if TYPE=='fields':    
  Y1_fdtd  =np.expand_dims(fobj['fdtd'],axis=1)
  w        =np.expand_dims(fobj['w'   ],axis=1)
  data     =np.concatenate((Y1_fdtd,w) ,axis=1).astype('float32')
 else:
  if TYPE=='bnd':
   data    =np.concatenate([np.expand_dims(fobj['bnd'] ,axis=1) for _ in range(TIME)],axis=1)
   data    =np.expand_dims(data,axis=4).astype('float32')
 return data

def saveEXPODDMTFRECORDS(HDFPATH,SHAPE,WRITEPATH,LIST,MODE='train'):
 filenames     =[os.path.join(HDFPATH,FILE) for FILE in rFILE(LIST)]  
 for i in range(len(filenames)):  
  savefilename =os.path.join(WRITEPATH,filenames[i].split('/')[6].split('.')[0]+'.tfrecords')
  img          =getEXPODDMDATASETS(filenames[i],'fields'         )
  bnd          =getEXPODDMDATASETS(filenames[i],'bnd'   ,SHAPE[0])   
  print(' WRITING ',filenames[i].split('/')[6].split('.')[0],' TO TFRECORDS FILE ')
  writer       =tf.io.TFRecordWriter(savefilename)
  for index in range(img.shape[0]):   
   if not np.sum(img[index][0,:,:,0])==0:                                                               # SKIP TOTALY EMPTY SUBDOMAINS
    img_example =np.reshape(img[index],np.prod(img[index].shape))
    bnd_example =np.reshape(bnd[index],np.prod(bnd[index].shape))
    feat={'images'  :tf.train.Feature(bytes_list=tf.train.BytesList(value=[img_example.tostring()])),
          'boundary':tf.train.Feature(bytes_list=tf.train.BytesList(value=[bnd_example.tostring()]))}  
    example     =tf.train.Example(features=tf.train.Features(feature=feat))
    writer.write(example.SerializeToString())   
  writer.close()

def inputEXPODDMTFRECORDS(DATAPATH,SHAPE,POPTIM,PBATCH,MODE='train'):
 if MODE=='train':                                                                                      # SET NUMBER OF EPOCHS
  epoch_num    =POPTIM[1]
 else:
  if MODE=='test':
   epoch_num   =POPTIM[4]
 def parse_fn(SERIALIZED_EXAMPLE):                                                                      # DEFINE PARSING FUNCTION
  FEAT         ={'images'  : tf.io.FixedLenFeature([],tf.string),
                 'boundary': tf.io.FixedLenFeature([],tf.string)}
  FEATURES     =tf.io.parse_single_example(SERIALIZED_EXAMPLE,features=FEAT) 
  _vid=tf.decode_raw(FEATURES['images'  ],tf.float32)
  _bnd=tf.decode_raw(FEATURES['boundary'],tf.float32)
  _vid.set_shape          ([np.prod(SHAPE)                   ])
  _bnd.set_shape          ([SHAPE[0]*SHAPE[1]*SHAPE[2]*1     ])
  vid          =tf.reshape(_vid, SHAPE                        )
  bnd          =tf.reshape(_bnd,[SHAPE[0],SHAPE[1],SHAPE[2],1])
  return vid,bnd
 filenames     =getFILENAMES(DATAPATH)     
 with tf.name_scope('READ'+'_'+'TFRecords'+'_'+MODE):                                                   # BEGIN READING DATA
  dataset      =tf.data.TFRecordDataset(filenames)
  dataset      =dataset.map(parse_fn)
  dataset      =dataset.shuffle(PBATCH[1])
  dataset      =dataset.repeat(epoch_num)
  dataset      =dataset.batch(PBATCH[0],drop_remainder=True)  
  iterator     =tf.compat.v1.data.make_one_shot_iterator(dataset)                                       # INITIALIZE ONE-SHOT ITERATOR  
  next_element =iterator.get_next()
  vid          =next_element[0]
  bnd          =next_element[1]
 return vid,bnd


