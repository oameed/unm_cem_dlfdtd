##################################
### UNIVERSITY OF NEW MEXICO   ###
### COMPUTATIONAL EM LAB       ###
### DEEP LEARNING PROJECT      ###
### LAYER FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN     ###
##################################

import numpy      as np
import tensorflow as tf

######################
### RESIDUAL LAYER ###
######################
def batch_norm(X,SHAPE,NAME,MODE,SCOPE=[],TYPE_CONV=True):
 if MODE=='train':
  with  tf.compat.v1.variable_scope('BN'+'_'+NAME,reuse=tf.compat.v1.AUTO_REUSE):
   beta                  =tf.compat.v1.get_variable  ('beta' ,SHAPE,initializer=tf.zeros_initializer(),trainable=True)
   gamma                 =tf.compat.v1.get_variable  ('gamma',SHAPE,initializer=tf.ones_initializer (),trainable=True)
   ema                   =tf.train.ExponentialMovingAverage(decay=0.995)
   if TYPE_CONV:
    batch_mean,batch_var =tf.nn.moments(X,[0,1,2])
   else:
    batch_mean,batch_var =tf.nn.moments(X,[0    ])
   ema_update_op         =ema.apply ([batch_mean,batch_var])
   ema_mean,ema_var      =ema.average(batch_mean),ema.average(batch_var)
   with tf.control_dependencies([ema_update_op]):
    x                    =tf.nn.batch_normalization(X,batch_mean,batch_var,beta,gamma,0.001)
 else:
  if MODE=='test':
   with tf.compat.v1.variable_scope('BN'+'_'+NAME,reuse=tf.compat.v1.AUTO_REUSE):
    beta                 =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=SCOPE+'/'+'BN'+'_'+NAME)[0]
    gamma                =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=SCOPE+'/'+'BN'+'_'+NAME)[1]
    ema_mean             =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.GLOBAL_VARIABLES   ,scope=SCOPE+'/'+'BN'+'_'+NAME)[2]
    ema_var              =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.GLOBAL_VARIABLES   ,scope=SCOPE+'/'+'BN'+'_'+NAME)[3]
    if TYPE_CONV:
     batch_mean,batch_var=tf.nn.moments(X,[0,1,2])
    else:
     batch_mean,batch_var=tf.nn.moments(X,[0    ])
    x                    =tf.nn.batch_normalization(X,batch_mean,batch_var,beta,gamma,0.001)
 return x
   
def res_block(X,num_in_ch,num_out_ch,NAME,MODE,LINEAR=False):
 if MODE=='train':
  with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
   INIT      =tf.contrib.layers.variance_scaling_initializer()
   if num_in_ch==num_out_ch:
    stride   =[1,1                      ]
    s        =X
   else:
    stride   =[2,2                      ]
    w_s_shape=[1,1,num_in_ch,num_out_ch ]
    w_s      =tf.compat.v1.get_variable( 'w_s'      ,w_s_shape ,initializer=INIT,trainable=True      )
    s        =tf.nn.conv2d             (X,w_s       ,strides=[1,stride[0],stride[1],1],padding='SAME')
    s        =batch_norm               (s,num_out_ch,'SHORTCUT',MODE                                 )
   w1shape   =[3,3,num_in_ch ,num_out_ch]
   w2shape   =[3,3,num_out_ch,num_out_ch]
   w1        =tf.compat.v1.get_variable( 'w1'       ,w1shape   ,initializer=INIT,trainable=True      )
   w2        =tf.compat.v1.get_variable( 'w2'       ,w2shape   ,initializer=INIT,trainable=True      )
   x         =tf.nn.conv2d             (X,w1        ,strides=[1,stride[0],stride[1],1],padding='SAME')
   x         =batch_norm               (x,num_out_ch,'FIRST'   ,MODE                                 )
   x         =tf.nn.relu               (x                                                            )
   x         =tf.nn.conv2d             (x,w2        ,strides=[1,1,1,1],padding='SAME'                )
   x         =batch_norm               (x,num_out_ch,'SECOND'  ,MODE                                 )
   x         =tf.add                   (x,s                                                          )
   if np.logical_not(LINEAR):
    x        =tf.nn.relu               (x                                                            )   
 else:
  if MODE=='test':
   with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
    if num_in_ch==num_out_ch:
     stride=[1,1]
     w1    =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[0]
     w2    =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[1]
     s     =X
    else:
     stride=[2,2]
     w_s   =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[0]
     w1    =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[3]
     w2    =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[4]
     s     =tf.nn.conv2d(X,w_s,strides=[1,stride[0],stride[1],1],padding='SAME')
     s     =batch_norm  (s,[] ,'SHORTCUT',MODE,NAME+'_'+'train'                ) 
    x      =tf.nn.conv2d(X,w1 ,strides=[1,stride[0],stride[1],1],padding='SAME')
    x      =batch_norm  (x,[] ,'FIRST'   ,MODE,NAME+'_'+'train'                )
    x      =tf.nn.relu  (x                                                     )
    x      =tf.nn.conv2d(x,w2 ,strides=[1,1        ,1        ,1],padding='SAME')
    x      =batch_norm  (x,[] ,'SECOND'  ,MODE,NAME+'_'+'train'                )
    x      =tf.add      (x,s                                                   )
    if np.logical_not(LINEAR):
     x     =tf.nn.relu(x                                                       )    
 return x

##################################
### LINEAR CONVOLUTIONAL LAYER ###
##################################
def conv_linear(X,num_in_ch,num_out_ch,NAME,MODE):                             # THIS LAYER IS USED AS THE LAST LAYER OF THE DECODER
 if MODE=='train':
  with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
   stride =[1,1                     ]
   wshape =[5,5,num_in_ch,num_out_ch]
   w      =tf.compat.v1.get_variable( 'w'        ,wshape ,trainable=True                          )
   x      =tf.nn.conv2d             (X,w         ,strides=[1,stride[0],stride[1],1],padding='SAME')
   x      =batch_norm               (x,num_out_ch,'FIRST',MODE                                    )
 else:
  if MODE=='test':
   with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
    stride=[1,1]
    w     =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[0]
    x     =tf.nn.conv2d               (X,w ,strides=[1,stride[0],stride[1],1],padding='SAME'            )
    x     =batch_norm                 (x,[],'FIRST',MODE,NAME+'_'+'train'                               )
 return x

##############################
### CONVOLUTION OPERNATION ###
##############################
def convolve(X,num_in_ch,num_out_ch,NAME,MODE):                                # THIS LAYER IS USED AS CONVOLUTION OPERATION INSIDE THE 2D LSTM CELL 
 if MODE=='train':
  with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
   stride =[1,1                     ]
   wshape =[3,3,num_in_ch,num_out_ch]
   w      =tf.compat.v1.get_variable( 'w',wshape,trainable=True                           )
   x      =tf.nn.conv2d             (X,w ,strides=[1,stride[0],stride[1],1],padding='SAME')
 else:
  if MODE=='test':
   with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
    stride=[1,1]
    w     =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[0]
    x     =tf.nn.conv2d               (X,w,strides=[1,stride[0],stride[1],1],padding='SAME'             )
 return x

####################################
### TRANSPOSED CONVOLUTION LAYER ###
####################################
def conv_trans_layer(X,num_in_ch,num_out_ch,OUTSHAPE,filt_shape,NAME,MODE):
 if MODE=='train':
  INIT       =tf.contrib.layers.variance_scaling_initializer()
  wshape     =[filt_shape[0],filt_shape[1],num_out_ch,num_in_ch]
  batch_size =X.get_shape().as_list()[0]
  outshape   =[batch_size,OUTSHAPE[0],OUTSHAPE[1],num_out_ch]
  with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
   w         =tf.compat.v1.get_variable( 'w',wshape    ,initializer=INIT,trainable=True )
   x         =tf.nn.conv2d_transpose   (X,w ,outshape  ,strides=[1,2,2,1],padding='SAME')
   x         =batch_norm               (x   ,num_out_ch,'FIRST',MODE                    )
   x         =tf.nn.relu               (x                                               )
 else:
  if MODE=='test':
   batch_size=X.get_shape().as_list()[0]
   outshape  =[batch_size,OUTSHAPE[0],OUTSHAPE[1],num_out_ch]
   with tf.compat.v1.variable_scope(NAME+'_'+MODE,reuse=tf.compat.v1.AUTO_REUSE):
    w        =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME+'_'+'train')[0]
    x        =tf.nn.conv2d_transpose     (X,w ,outshape,strides=[1,2,2,1],padding='SAME'                    )
    x        =batch_norm                 (x,[],'FIRST',MODE,NAME+'_'+'train'                                )
    x        =tf.nn.relu                 (x                                                                 )
 return x

######################################################################################
### RNN-CELL                                                                       ###
### PREV[0]: PREVIOUS CELL   STATE                                                 ###
### PREV[1]: PREVIOUS  HIDDEN STATE                                                ###
### IN OUR APPLICATION,'PREV' AND 'X' ARE OF THE SAME SHAPE IN NON-TIME DIMENSIONS ###
######################################################################################
def rnn(B_SUM,B_MUL,MODE):                                                     # THIS IS AN LSTM CELL MODIFIED TO HANDLE BOUNDARY DATA
 def rnn_cell_b(PREV,X,NAME='RNN_CELL_B'):  
  PREV[1]=tf.add(tf.multiply(PREV[1],B_MUL),B_SUM)                             # MIX PREVIOUS HIDDEN STATE WITH BOUNDARY  
  shape  =PREV[1].get_shape().as_list()[-1]                                    # GENERATE CURRENT CELL/HIDDEN STATES
  with tf.compat.v1.variable_scope(NAME,reuse=tf.compat.v1.AUTO_REUSE):   
   if MODE=='train':                                                           # GET BIAS VARIABLES
    bI   =tf.compat.v1.get_variable('bI', shape,trainable=True)
    bgi  =tf.compat.v1.get_variable('bgi',shape,trainable=True)
    bgf  =tf.compat.v1.get_variable('bgf',shape,trainable=True)
    bgo  =tf.compat.v1.get_variable('bgo',shape,trainable=True)
   else:
    if MODE=='test':
     bI  =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME)[0]
     bgi =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME)[1]
     bgf =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME)[2]
     bgo =tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES,scope=NAME)[3]
                                                                               # APPLY LSTM EQUATIONS
                                                                               # PROCESS INPUT
  I      =tf.add        ( convolve(X,shape,shape,'WxI', MODE),convolve(PREV[1],shape,shape,'WhI', MODE))
  I      =tf.nn.bias_add(I,bI)
  I      =tf.nn.tanh    (I   )
                                                                               # INPUT  GATE
  gi     =tf.add_n      ([convolve(X,shape,shape,'Wxgi',MODE),convolve(PREV[1],shape,shape,'Whgi',MODE),convolve(PREV[0],shape,shape,'Wcgi',MODE)])
  gi     =tf.nn.bias_add(gi,bgi)
  gi     =tf.nn.sigmoid (gi    )
                                                                               # FORGET GATE
  gf     =tf.add_n      ([convolve(X,shape,shape,'Wxgf',MODE),convolve(PREV[1],shape,shape,'Whgf',MODE),convolve(PREV[0],shape,shape,'Wcgf',MODE)])
  gf     =tf.nn.bias_add(gf,bgf)
  gf     =tf.nn.sigmoid (gf    )
                                                                               # CURRENT CELL   STATE
  cs     =tf.add(tf.multiply(gf,PREV[0]),tf.multiply(gi,I))
                                                                               # OUTPUT GATE
  go     =tf.add_n      ([convolve(X,shape,shape,'Wxgo',MODE),convolve(PREV[1],shape,shape,'Whgo',MODE),convolve(cs,     shape,shape,'Wcgo',MODE)])
  go     =tf.nn.bias_add(go,bgo)
  go     =tf.nn.sigmoid (go    )
                                                                               # CURRENT HIDDEN STATE
  hs     =tf.multiply   (go,tf.nn.tanh(cs))
  CURR   =[cs,hs]
  return CURR
 return rnn_cell_b


