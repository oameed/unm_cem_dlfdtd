%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                     %%%
%%% COMPUTATIONAL EM LAB                         %%%
%%% DEEP LEARNING PROJECT                        %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD %%%
%%% by: OAMEED NOAKOASTEEN                       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [EX,EY,HZ]=get2DfromV1D(VECTOR,LGBN,PRED)
SIZE=sqrt(size(VECTOR,1)/5);
EX  =zeros(SIZE       )    ;
EY  =zeros(SIZE       )    ;
HZ  =zeros(SIZE,SIZE,3)    ;
for i=1:SIZE*SIZE    
    prev_end_index           =(i-1)*5                 ;
    EX(LGBN(i,3),LGBN(i,4)  )=VECTOR(prev_end_index+1);
    EY(LGBN(i,3),LGBN(i,4)  )=VECTOR(prev_end_index+2);
    HZ(LGBN(i,3),LGBN(i,4),1)=VECTOR(prev_end_index+3);
    HZ(LGBN(i,3),LGBN(i,4),2)=VECTOR(prev_end_index+4);
    HZ(LGBN(i,3),LGBN(i,4),3)=VECTOR(prev_end_index+5);
end

if PRED
    EX=get_corrected_subdom_order(EX);
    EY=get_corrected_subdom_order(EY);
    HZ=get_corrected_subdom_order(HZ);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function x=get_corrected_subdom_order(X)        
        if size(X,1)==256
            x                   =zeros(size(X))          ;
            subdom_1            =X(1:128  ,1:128  ,:)    ;
            subdom_2            =X(129:256,1:128  ,:)    ;
            subdom_3            =X(1:128  ,129:256,:)    ;
            subdom_4            =X(129:256,129:256,:)    ;
            x(1:128  ,1:128  ,:)=subdom_1                ;
            x(129:256,1:128  ,:)=subdom_3                ;
            x(1:128  ,129:256,:)=subdom_2                ;
            x(129:256,129:256,:)=subdom_4                ;
        else
            if size(X,1)==512
                x                   =zeros(size(X))      ;
                subdom_1            =X(1  :128,1:128  ,:);
                subdom_2            =X(129:256,1:128  ,:);
                subdom_3            =X(257:384,1:128  ,:);
                subdom_4            =X(385:512,1:128  ,:);
                subdom_5            =X(1  :128,129:256,:);
                subdom_6            =X(129:256,129:256,:);
                subdom_7            =X(257:384,129:256,:);
                subdom_8            =X(385:512,129:256,:);
                subdom_9            =X(1  :128,257:384,:);
                subdom_10           =X(129:256,257:384,:);
                subdom_11           =X(257:384,257:384,:);
                subdom_12           =X(385:512,257:384,:);
                subdom_13           =X(1  :128,385:512,:);
                subdom_14           =X(129:256,385:512,:);
                subdom_15           =X(257:384,385:512,:);
                subdom_16           =X(385:512,385:512,:);                
                x(1  :128,1:128  ,:)=subdom_1            ;
                x(129:256,1:128  ,:)=subdom_5            ;
                x(257:384,1:128  ,:)=subdom_9            ;
                x(385:512,1:128  ,:)=subdom_13           ;
                x(1  :128,129:256,:)=subdom_2            ;
                x(129:256,129:256,:)=subdom_6            ;
                x(257:384,129:256,:)=subdom_10           ;
                x(385:512,129:256,:)=subdom_14           ;
                x(1  :128,257:384,:)=subdom_3            ;
                x(129:256,257:384,:)=subdom_7            ;
                x(257:384,257:384,:)=subdom_11           ;
                x(385:512,257:384,:)=subdom_15           ;
                x(1  :128,385:512,:)=subdom_4            ;
                x(129:256,385:512,:)=subdom_8            ;
                x(257:384,385:512,:)=subdom_12           ;
                x(385:512,385:512,:)=subdom_16           ;
            end
        end
    end

end

