%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                     %%%
%%% COMPUTATIONAL EM LAB                         %%%
%%% DEEP LEARNING PROJECT                        %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD %%%
%%% by: OAMEED NOAKOASTEEN                       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% list_glb_num (1): GLOBAL NUMBER
% list_glb_num (2): DOMAIN NUMBER
% list_glb_num (3): GLOBAL ROW INDEX
% list_glb_num (4): GLOBAL_COL_INDEX

function list_glb_num=get_global_number_list(SIZE,DDMDIV)
numROW      =SIZE(1)                                                                    ;
numCOL      =SIZE(2)                                                                    ;
numROW_DDM  =SIZE(1)/DDMDIV                                                             ;
numCOL_DDM  =SIZE(2)/DDMDIV                                                             ;
domain_dofs =numROW_DDM*numCOL_DDM                                                      ;
list_glb_num=[]                                                                         ;
list_ngbr   ={}                                                                         ;
for i=1:numROW
    for j=1:numCOL                                 
        domain_i         =ceil(i/numROW_DDM)                                            ;
        domain_j         =ceil(j/numCOL_DDM)                                            ;        
        domain_total_prev=((domain_i-1)*DDMDIV+(domain_j-1))*domain_dofs                ;
        domain_i_local   =i-(domain_i-1)*numROW_DDM                                     ;
        domain_j_local   =j-(domain_j-1)*numCOL_DDM                                     ;
        global_num       =domain_total_prev+(domain_i_local-1)*numCOL_DDM+domain_j_local;        
        list_glb_num     =[list_glb_num;global_num,(domain_i-1)*DDMDIV+domain_j,i,j]    ;        
    end
end
list_glb_num=sortrows(list_glb_num,1)                                                   ;
end

