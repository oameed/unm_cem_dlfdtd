%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                     %%%
%%% COMPUTATIONAL EM LAB                         %%%
%%% DEEP LEARNING PROJECT                        %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD %%%
%%% by: OAMEED NOAKOASTEEN                       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function vector = getV1Dfrom2D(EX,EY,HZ,LGBN)
SIZE   = 5*size(EX,1)*size(EX,2)                   ;
vector = zeros(SIZE,1)                             ;
list   = LGBN(:,[3,4])                             ;
for i=1:size(EX,1)
    for j=1:size(EX,2)
        [logic,index]=ismember([i,j],list,'rows')  ;
        if logic
            prev_end_index            =(index-1)*5 ;
            vector(prev_end_index+1,1)=EX(i,j  )   ;
            vector(prev_end_index+2,1)=EY(i,j  )   ;
            vector(prev_end_index+3,1)=HZ(i,j,1)   ;
            vector(prev_end_index+4,1)=HZ(i,j,2)   ;
            vector(prev_end_index+5,1)=HZ(i,j,3)   ;
        end
    end
end
end