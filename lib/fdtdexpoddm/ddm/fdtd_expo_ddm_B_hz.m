%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                            %%%
%%% COMPUTATIONAL EM LAB                                                %%%
%%% DEEP LEARNING PROJECT                                               %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD                        %%%
%%% 2D FDTD / PRECONDITIONING MATRIX ('B') / APPROXIMATION MATRIX ('E') %%%
%%% by: OAMEED NOAKOASTEEN, SHU WANG                                    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function HzTRUE=fdtd_expo_ddm_B_hz(Ex,Ey,Hz,Chz,Ndy,Ndx,NPMLC,list_glb_num)
HzE=Hz;
ExE=Ex;
EyE=Ey;
%%%
for j=2:Ndy-1
    for i=2:Ndx-1
        point_self_index     =find((list_glb_num(:,3)==j    )&(list_glb_num(:,4)==i   ))     ;
        point_self_domain_num=list_glb_num(point_self_index,2)                               ;
        point_1_index        =find((list_glb_num(:,3)==(j+1))&(list_glb_num(:,4)==i   ))     ;
        point_1_domain_num   =list_glb_num(point_1_index   ,2)                               ;
        point_3_index        =find((list_glb_num(:,3)==(j  ))&(list_glb_num(:,4)==(i+1)))    ;
        point_3_domain_num   =list_glb_num(point_3_index   ,2)                               ;
        if (NPMLC<i && i<size(Hz,2)-NPMLC)&&(NPMLC<j && j<size(Hz,1)-NPMLC)
            Hz(j,i,1)=Chz(j,i,1)*Hz(j,i,1)  +...
                      Chz(j,i,2)*(0-Ex(j,i))+...
                      Chz(j,i,3)*(0-Ey(j,i))                                                 ;
            if (point_1_domain_num==point_self_domain_num)
                Hz(j,i,1)=Hz(j,i,1)+Chz(j,i,2)*Ex(j+1,i)                                     ;
            end
            if (point_3_domain_num==point_self_domain_num)
                 Hz(j,i,1)=Hz(j,i,1)+Chz(j,i,3)*Ey(j,i+1)                                    ;
            end
        else
            Hz(j,i,2)=Chz(j,i,5)*Hz(j,i,2)  +...
                      Chz(j,i,8)*(0-Ey(j,i))                                                 ;
            Hz(j,i,3)=Chz(j,i,6)*Hz(j,i,3)  +...
                      Chz(j,i,7)*(0-Ex(j,i))                                                 ; 
            if (point_3_domain_num==point_self_domain_num)
                Hz(j,i,2)=Hz(j,i,2)+Chz(j,i,8)*Ey(j,i+1)                                     ;
            end
            if (point_1_domain_num==point_self_domain_num)
                Hz(j,i,3)=Hz(j,i,3)+Chz(j,i,7)*Ex(j+1,i)                                     ;
            end
            Hz(j,i,1)=Hz(j,i,2)+Hz(j,i,3)                                                    ;
        end
    end
end
%%%%
for j=2:Ndy-1
    for i=2:Ndx-1
        point_self_index     =find((list_glb_num(:,3)==j    )&(list_glb_num(:,4)==i   ))     ;
        point_self_domain_num=list_glb_num(point_self_index,2)                               ;
        point_1_index        =find((list_glb_num(:,3)==(j+1))&(list_glb_num(:,4)==i   ))     ;
        point_1_domain_num   =list_glb_num(point_1_index   ,2)                               ;
        point_3_index        =find((list_glb_num(:,3)==(j  ))&(list_glb_num(:,4)==(i+1)))    ;
        point_3_domain_num   =list_glb_num(point_3_index   ,2)                               ;
        if (NPMLC<i && i<size(HzE,2)-NPMLC)&&(NPMLC<j && j<size(HzE,1)-NPMLC)
            HzE(j,i,1)=Chz(j,i,1)*HzE(j,i,1)+...
                       Chz(j,i,2)*(0-0)     +...
                       Chz(j,i,3)*(0-0)                                                      ;
            if ~(point_1_domain_num==point_self_domain_num)
                HzE(j,i,1)=HzE(j,i,1)+Chz(j,i,2)*ExE(j+1,i)                                  ;
            end
            if ~(point_3_domain_num==point_self_domain_num)
                HzE(j,i,1)=HzE(j,i,1)+Chz(j,i,3)*EyE(j,i+1)                                  ;
            end
        else
            HzE(j,i,2)=Chz(j,i,5)*HzE(j,i,2)  +...
                       Chz(j,i,8)*(0-0)                                                      ;
            HzE(j,i,3)=Chz(j,i,6)*HzE(j,i,3)  +...
                       Chz(j,i,7)*(0-0)                                                      ;
            if ~(point_3_domain_num==point_self_domain_num)
                HzE(j,i,2)=HzE(j,i,2)+Chz(j,i,8)*EyE(j,i+1)                                  ;
            end
            if ~(point_1_domain_num==point_self_domain_num)
                HzE(j,i,3)=HzE(j,i,3)+Chz(j,i,7)*ExE(j+1,i)                                  ;
            end
            HzE(j,i,1)=HzE(j,i,2)+HzE(j,i,3)                                                 ;
        end
    end
end                
%%%%%
HzTRUE=Hz+HzE                                                                                ;
end

