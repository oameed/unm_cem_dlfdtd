%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                            %%%
%%% COMPUTATIONAL EM LAB                                                %%%
%%% DEEP LEARNING PROJECT                                               %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD                        %%%
%%% 2D FDTD / PRECONDITIONING MATRIX ('B') / APPROXIMATION MATRIX ('E') %%%
%%% by: OAMEED NOAKOASTEEN, SHU WANG                                    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ex=fdtd_expo_ddm_E_ex(Ex,HzTRUE,Cex,Ndy,Ndx,list_glb_num)
for j=2:Ndy
    for i=1:Ndx
        point_self_index     =find((list_glb_num(:,3)==j    )&(list_glb_num(:,4)==i   )) ;
        point_self_domain_num=list_glb_num(point_self_index,2)                           ;
        point_2_index        =find((list_glb_num(:,3)==(j-1))&(list_glb_num(:,4)==i   )) ;
        point_2_domain_num   =list_glb_num(point_2_index   ,2)                           ;
        Ex(j,i)=Cex(j,i,1)*Ex(j,i)    +...
                Cex(j,i,2)*(0-0)                                                         ;
        if ~(point_2_domain_num==point_self_domain_num)
            Ex(j,i)=Ex(j,i)+Cex(j,i,2)*(-HzTRUE(j-1,i))                                  ;
        end                        
    end
end
end

