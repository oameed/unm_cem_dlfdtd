%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                            %%%
%%% COMPUTATIONAL EM LAB                                                %%%
%%% DEEP LEARNING PROJECT                                               %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD                        %%%
%%% 2D FDTD / PRECONDITIONING MATRIX ('B') / APPROXIMATION MATRIX ('E') %%%
%%% by: OAMEED NOAKOASTEEN, SHU WANG                                    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function HzTRUE=fdtd_expo_ddm_E_hz(Ex,Ey,Hz,Chz,Ndy,Ndx,NPMLC,list_glb_num)
HzB=Hz;
ExB=Ex;
EyB=Ey;
%%%
for j=2:Ndy-1
    for i=2:Ndx-1
        point_self_index     =find((list_glb_num(:,3)==j    )&(list_glb_num(:,4)==i   )) ;
        point_self_domain_num=list_glb_num(point_self_index,2)                           ;
        point_1_index        =find((list_glb_num(:,3)==(j+1))&(list_glb_num(:,4)==i   )) ;
        point_1_domain_num   =list_glb_num(point_1_index   ,2)                           ;
        point_3_index        =find((list_glb_num(:,3)==(j  ))&(list_glb_num(:,4)==(i+1)));
        point_3_domain_num   =list_glb_num(point_3_index   ,2)                           ;
        if (NPMLC<i && i<size(Hz,2)-NPMLC)&&(NPMLC<j && j<size(Hz,1)-NPMLC)
            Hz(j,i,1)=Chz(j,i,1)*Hz(j,i,1)  +...
                      Chz(j,i,2)*(0-0)+...
                      Chz(j,i,3)*(0-0)                                                   ;
            if ~(point_1_domain_num==point_self_domain_num)
                Hz(j,i,1)=Hz(j,i,1)+Chz(j,i,2)*Ex(j+1,i)                                 ;
            end
            if ~(point_3_domain_num==point_self_domain_num)
                Hz(j,i,1)=Hz(j,i,1)+Chz(j,i,3)*Ey(j,i+1)                                 ;
            end
        else
            Hz(j,i,2)=Chz(j,i,5)*Hz(j,i,2)  +...
                      Chz(j,i,8)*(0-0)                                                   ;
            Hz(j,i,3)=Chz(j,i,6)*Hz(j,i,3)  +...
                      Chz(j,i,7)*(0-0)                                                   ;
            if ~(point_3_domain_num==point_self_domain_num)
                Hz(j,i,2)=Hz(j,i,2)+Chz(j,i,8)*Ey(j,i+1)                                 ;
            end
            if ~(point_1_domain_num==point_self_domain_num)
                Hz(j,i,3)=Hz(j,i,3)+Chz(j,i,7)*Ex(j+1,i)                                 ;
            end
            Hz(j,i,1)=Hz(j,i,2)+Hz(j,i,3)                                                ;
        end
    end
end
%%%
for j=2:Ndy-1
    for i=2:Ndx-1                    
        point_self_index     =find((list_glb_num(:,3)==j    )&(list_glb_num(:,4)==i   )) ;
        point_self_domain_num=list_glb_num(point_self_index,2)                           ;
        point_1_index        =find((list_glb_num(:,3)==(j+1))&(list_glb_num(:,4)==i   )) ;
        point_1_domain_num   =list_glb_num(point_1_index   ,2)                           ;
        point_3_index        =find((list_glb_num(:,3)==(j  ))&(list_glb_num(:,4)==(i+1)));
        point_3_domain_num   =list_glb_num(point_3_index   ,2)                           ;
        if (NPMLC<i && i<size(HzB,2)-NPMLC)&&(NPMLC<j && j<size(HzB,1)-NPMLC)
            HzB(j,i,1)=Chz(j,i,1)*HzB(j,i,1)  +...
                      Chz(j,i,2)*(0-ExB(j,i))+...
                      Chz(j,i,3)*(0-EyB(j,i))                                            ;
            if (point_1_domain_num==point_self_domain_num)
                HzB(j,i,1)=HzB(j,i,1)+Chz(j,i,2)*ExB(j+1,i)                              ;
            end
            if (point_3_domain_num==point_self_domain_num)
                 HzB(j,i,1)=HzB(j,i,1)+Chz(j,i,3)*EyB(j,i+1)                             ;
            end
        else
            HzB(j,i,2)=Chz(j,i,5)*HzB(j,i,2)  +...
                      Chz(j,i,8)*(0-EyB(j,i))                                            ;
            HzB(j,i,3)=Chz(j,i,6)*HzB(j,i,3)  +...
                      Chz(j,i,7)*(0-ExB(j,i))                                            ; 
            if (point_3_domain_num==point_self_domain_num)
                HzB(j,i,2)=HzB(j,i,2)+Chz(j,i,8)*EyB(j,i+1)                              ;
            end
            if (point_1_domain_num==point_self_domain_num)
                HzB(j,i,3)=HzB(j,i,3)+Chz(j,i,7)*ExB(j+1,i)                              ;
            end
            HzB(j,i,1)=HzB(j,i,2)+HzB(j,i,3)                                             ;
        end
    end
end
HzTRUE=Hz+HzB;
%%%
end

