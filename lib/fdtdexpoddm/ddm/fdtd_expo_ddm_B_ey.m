%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                            %%%
%%% COMPUTATIONAL EM LAB                                                %%%
%%% DEEP LEARNING PROJECT                                               %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD                        %%%
%%% 2D FDTD / PRECONDITIONING MATRIX ('B') / APPROXIMATION MATRIX ('E') %%%
%%% by: OAMEED NOAKOASTEEN, SHU WANG                                    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ey=fdtd_expo_ddm_B_ey(Ey,HzTRUE,Cey,Ndy,Ndx,list_glb_num)
for j=1:Ndy
    for i=2:Ndx
        point_self_index     =find((list_glb_num(:,3)==j    )&(list_glb_num(:,4)==i   ))     ;
        point_self_domain_num=list_glb_num(point_self_index,2)                               ;
        point_2_index        =find((list_glb_num(:,3)==(j  ))&(list_glb_num(:,4)==i-1 ))     ;
        point_2_domain_num   =list_glb_num(point_2_index   ,2)                               ;
        Ey(j,i)=Cey(j,i,1)*Ey(j,i)    +...
                Cey(j,i,2)*(HzTRUE(j,i)-0)                                                   ;
        if (point_2_domain_num==point_self_domain_num)
            Ey(j,i)=Ey(j,i)+Cey(j,i,2)*(-HzTRUE(j,i-1))                                      ;
        end
    end
end
end

