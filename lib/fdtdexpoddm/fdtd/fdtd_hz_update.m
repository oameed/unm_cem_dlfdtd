%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                            %%%
%%% COMPUTATIONAL EM LAB                                                %%%
%%% DEEP LEARNING PROJECT                                               %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD                        %%%
%%% 2D FDTD / PRECONDITIONING MATRIX ('B') / APPROXIMATION MATRIX ('E') %%%
%%% by: OAMEED NOAKOASTEEN -- SHU WANG                                  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Hz=fdtd_hz_update(Ex,Ey,Hz,Chz,Ndy,Ndx,NPMLC)
for j=2:Ndy-1    
    for i=2:Ndx-1                
        if (NPMLC<i && i<size(Hz,2)-NPMLC)&&(NPMLC<j && j<size(Hz,1)-NPMLC)
            Hz(j,i,1)=Chz(j,i,1)*Hz(j,i,1)          +...
                      Chz(j,i,2)*(Ex(j+1,i)-Ex(j,i))+...
                      Chz(j,i,3)*(Ey(j,i+1)-Ey(j,i))                                             ;
        else
            Hz(j,i,2)=Chz(j,i,5)*Hz(j,i,2)          +...
                      Chz(j,i,8)*(Ey(j,i+1)-Ey(j,i))                                             ;
            Hz(j,i,3)=Chz(j,i,6)*Hz(j,i,3)          +...
                      Chz(j,i,7)*(Ex(j+1,i)-Ex(j,i))                                             ;
            Hz(j,i,1)=Hz(j,i,2)+Hz(j,i,3)                                                        ;
        end
    end
end
end

