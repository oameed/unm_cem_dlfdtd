%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                            %%%
%%% COMPUTATIONAL EM LAB                                                %%%
%%% DEEP LEARNING PROJECT                                               %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD                        %%%
%%% 2D FDTD / PRECONDITIONING MATRIX ('B') / APPROXIMATION MATRIX ('E') %%%
%%% by: OAMEED NOAKOASTEEN -- SHU WANG                                  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ey=fdtd_ey_update(Ey,Hz,Cey,Ndy,Ndx)
for j=1:Ndy
    for i=2:Ndx
        Ey(j,i)=Cey(j,i,1)*Ey(j,i)            +...
                Cey(j,i,2)*(Hz(j,i)-Hz(j,i-1))                                                   ;
    end
end
end

