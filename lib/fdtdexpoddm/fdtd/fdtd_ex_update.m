%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                            %%%
%%% COMPUTATIONAL EM LAB                                                %%%
%%% DEEP LEARNING PROJECT                                               %%%
%%% MATRIX EXPONENTIAL DOMAIN DECOMPOSITION FDTD                        %%%
%%% 2D FDTD / PRECONDITIONING MATRIX ('B') / APPROXIMATION MATRIX ('E') %%%
%%% by: OAMEED NOAKOASTEEN -- SHU WANG                                  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ex=fdtd_ex_update(Ex,Hz,Cex,Ndy,Ndx)
for j=2:Ndy
    for i=1:Ndx
        Ex(j,i)=Cex(j,i,1)*Ex(j,i)            +...
                Cex(j,i,2)*(Hz(j,i)-Hz(j-1,i))                                                   ;
    end
end
end

