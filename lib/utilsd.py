######################################
### UNIVERSITY OF NEW MEXICO       ###
### COMPUTATIONAL EM LAB           ###
### DEEP LEARNING PROJECT          ###
### UTILITIES FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN         ###
######################################

import os
import numpy      as np
import tensorflow as tf
import h5py

def getFILENAMES(PATH,COMPLETE=True):
 filename=[]
 for FILE in os.listdir(PATH):
  if not FILE.startswith('.'):
   if COMPLETE:
    filename.append(os.path.join(PATH,FILE)) 
   else:
    filename.append(FILE)
 return filename

def sortFILENAMES(FILENAMES,EXPODDM): 
 filenames=[]
 if not EXPODDM:
  sortlist =np.array([])  
  for i in range(len(FILENAMES)):
   filename=FILENAMES[i]
   sortlist=np.append(sortlist,int(filename.split('_')[1].split('.')[0]))
  sortlist =np.sort(sortlist.astype('int'))
  for i in range(len(FILENAMES)):
   filenames.append('simulation'+'_'+str(sortlist[i])+'.h5')
 else:
  sortlist=[]
  for i in range(len(FILENAMES)):
   filename=FILENAMES[i]
   sortlist.append([int(filename.split('_')[1]),int(filename.split('_')[2].split('.')[0])])
  sortlist.sort()
  for i in range(len(FILENAMES)):
   filenames.append('simulation'+'_'+str(sortlist[i][0])+'_'+str(sortlist[i][1])+'.h5')
 return filenames

def wFILE(SAVEPATH,FILENAME,DATA):
 with open(os.path.join(SAVEPATH,FILENAME),'w') as file:
  for i in range(len(DATA)):
   file.write(DATA[i]+'\n')

def rFILE(FILENAME):
 namelist=[]
 with open(FILENAME) as file:
  for line in file:
   namelist.append(line.strip('\n'))
 return namelist

def rHDF(FILENAME):
 fobj=h5py.File(FILENAME,'r')
 keys=[key for key in fobj.keys()]
 return fobj,keys

def wHDF(FILENAME,DIRNAMES,DATA):
 fobj=h5py.File(FILENAME,'w')
 for i in range(len(DIRNAMES)):
  fobj.create_dataset(DIRNAMES[i],data=DATA[i])
 fobj.close()
 
def cleanup(PATH):
 for FILE in os.listdir(PATH):
  if not FILE.startswith('.'):   
   FILE_PATH=os.path.join(PATH,FILE)
   os.remove(FILE_PATH)

def write_summaries_image(DESC,X,BND=False,MODE='train',NAME='SUMMARIES'):
 def summary(DATA,DESC,TYPE,TIME):
  for i in range(TIME):
   tf.compat.v1.summary.image(DESC+'_'+TYPE+'_'+str(i+1),DATA[:,i],max_outputs=1)
 SHAPE=X.get_shape().as_list()
 time =SHAPE[1]
 chnl =SHAPE[4]
 eta  =tf.sqrt(tf.divide(tf.constant(4*np.pi*1e-7),tf.constant(8.85e-12)))
 if np.logical_not(BND):
  if chnl==1:
   with tf.name_scope(NAME+'_'+MODE):
    hz=tf.divide(X,eta)
    # PLOT Hz
    summary(hz,DESC,'Hz',time)
  else:
   if chnl==2:
    with tf.name_scope(NAME+'_'+MODE):
     ex=X[:,:,:,:,0]
     ey=X[:,:,:,:,1]
     E =tf.sqrt(tf.add(tf.pow(ex,2),tf.pow(ey,2)))
     ex=tf.expand_dims(ex,axis=-1)
     ey=tf.expand_dims(ey,axis=-1)
     E =tf.expand_dims(E ,axis=-1)
     # PLOT MAG E
     summary(E ,DESC,'E' ,time)
     # PLOT Ex
     summary(ex,DESC,'Ex',time)
     # PLOT Ey
     summary(ey,DESC,'Ey',time)
   else:
    if chnl==3:
     with tf.name_scope(NAME+'_'+MODE):
      ex=X[:,:,:,:,0]
      ey=X[:,:,:,:,1]
      hz=tf.divide(X[:,:,:,:,2],eta)
      E =tf.sqrt(tf.add(tf.pow(ex,2),tf.pow(ey,2)))
      P =(0.5)*tf.multiply(tf.abs(hz),E)
      ex=tf.expand_dims(ex,axis=-1)
      ey=tf.expand_dims(ey,axis=-1)
      hz=tf.expand_dims(hz,axis=-1)
      E =tf.expand_dims(E ,axis=-1)
      P =tf.expand_dims(P ,axis=-1)
      # PLOT POWER
      summary(P ,DESC,'Power',time)
      # PLOT MAG E
      summary(E ,DESC,'E'    ,time)
      # PLOT Ex
      summary(ex,DESC,'Ex'   ,time)
      # PLOT Ey
      summary(ey,DESC,'Ey'   ,time)
      # PLOT Hz
      summary(hz,DESC,'Hz'   ,time)
    else:
     if chnl==5:
      with tf.name_scope(NAME+'_'+MODE):      
       ex =          X[:,:,:,:,0]
       ey =          X[:,:,:,:,1]
       hz =tf.divide(X[:,:,:,:,2],eta)
       hzx=tf.divide(X[:,:,:,:,3],eta)
       hzy=tf.divide(X[:,:,:,:,4],eta)
       E  =tf.sqrt(tf.add(tf.pow(ex,2),tf.pow(ey,2)))
       P  =(0.5)*tf.multiply(tf.abs(hz),E)
       ex =tf.expand_dims(ex ,axis=-1)
       ey =tf.expand_dims(ey ,axis=-1)
       hz =tf.expand_dims(hz ,axis=-1)
       hzx=tf.expand_dims(hzx,axis=-1)
       hzy=tf.expand_dims(hzy,axis=-1)
       E  =tf.expand_dims(E  ,axis=-1)
       P  =tf.expand_dims(P  ,axis=-1)
       # PLOT POWER
       summary(P  ,DESC,'Power',time)
       # PLOT MAG E
       summary(E  ,DESC,'E'    ,time)
       # PLOT Ex
       summary(ex ,DESC,'Ex'   ,time)
       # PLOT Ey
       summary(ey ,DESC,'Ey'   ,time)
       # PLOT Hz
       summary(hz ,DESC,'Hz'   ,time)
       # PLOT Hzx
       summary(hzx,DESC,'Hzx'  ,time)
       # PLOT Hzy
       summary(hzy,DESC,'Hzy'  ,time)      
 else:
  with tf.name_scope(NAME+'_'+MODE):
   summary(X,DESC,'OBJECTS',time) 

def ckptREAD(CKPTPATH,PRINT=False):
 f   =open(os.path.join(CKPTPATH,'checkpoint'),'r')
 fl  =f.readlines()
 CKPT=fl[0].split('"')[1]
 if PRINT:
  print(' READING CHECKPOINT: ',CKPT)
 return CKPT

def getPARAMS(X,TYPE):
 def getDATASETS(X):
  SHAPE  =X.get_shape().as_list()
  dataset=[]
  for i in range(SHAPE[3]):
   dataset.append(X[:,:,:,i])
  return dataset
 DATA =getDATASETS(X)
 param=[] 
 if len(DATA)==1:
  if TYPE=='scale':
   param.append(tf.reduce_max(tf.abs(DATA[0]),(1,2)))
  else:
   if TYPE=='moments':
    param.append(tf.nn.moments(DATA[0],(1,2)))
   else:
    if TYPE=='min':
     param.append(tf.reduce_min(DATA[0],(1,2)))
    else:
     if TYPE=='max': 
      param.append(tf.reduce_max(DATA[0],(1,2))) 
 else:
  if len(DATA)==2:
   if TYPE=='scale':
    #mag=tf.reduce_max(tf.sqrt(tf.add(tf.pow(DATA[0],2),tf.pow(DATA[1],2))),(1,2))
    param.append(tf.reduce_max(tf.abs(DATA[0]),(1,2)))
    param.append(tf.reduce_max(tf.abs(DATA[1]),(1,2)))
   else:
    if TYPE=='moments':
     param.append(tf.nn.moments(DATA[0],(1,2))) 
     param.append(tf.nn.moments(DATA[1],(1,2))) 
    else:
     if TYPE=='min':
      param.append(tf.reduce_min(DATA[0],(1,2)))
      param.append(tf.reduce_min(DATA[1],(1,2))) 
     else:
      if TYPE=='max':
       param.append(tf.reduce_max(DATA[0],(1,2)))
       param.append(tf.reduce_max(DATA[1],(1,2))) 
  else:
   if len(DATA)==3:
     if TYPE=='scale':
      #mag=tf.reduce_max(tf.sqrt(tf.add(tf.pow(DATA[0],2),tf.pow(DATA[1],2))),(1,2))
      param.append(tf.reduce_max(tf.abs(DATA[0]),(1,2)))
      param.append(tf.reduce_max(tf.abs(DATA[1]),(1,2)))
      param.append(tf.reduce_max(tf.abs(DATA[2]),(1,2)))
     else:
      if TYPE=='moments':
       param.append(tf.nn.moments(DATA[0],(1,2))) 
       param.append(tf.nn.moments(DATA[1],(1,2)))
       param.append(tf.nn.moments(DATA[2],(1,2)))
      else:
       if TYPE=='min':
        param.append(tf.reduce_min(DATA[0],(1,2)))
        param.append(tf.reduce_min(DATA[1],(1,2)))
        param.append(tf.reduce_min(DATA[2],(1,2))) 
       else:
        if TYPE=='max':
         param.append(tf.reduce_max(DATA[0],(1,2)))
         param.append(tf.reduce_max(DATA[1],(1,2)))
         param.append(tf.reduce_max(DATA[2],(1,2))) 
   else:
    if len(DATA)==5:
     # THIS IS A SPECIAL CASE FOR EXPO-DDM ONLY
     if TYPE=='scale':
      param.append(tf.reduce_max(tf.abs(DATA[0]),(1,2)))
      param.append(tf.reduce_max(tf.abs(DATA[1]),(1,2)))
      param.append(tf.reduce_max(tf.abs(DATA[2]),(1,2)))
      param.append(tf.reduce_max(tf.abs(DATA[3]),(1,2)))
      param.append(tf.reduce_max(tf.abs(DATA[4]),(1,2)))      
     else:
      if TYPE=='moments':
       param.append(tf.nn.moments(DATA[0],(1,2))) 
       param.append(tf.nn.moments(DATA[1],(1,2)))
       param.append(tf.nn.moments(DATA[2],(1,2)))
       param.append(tf.nn.moments(DATA[3],(1,2)))
       param.append(tf.nn.moments(DATA[4],(1,2)))       
      else:
       if TYPE=='min':
        param.append(tf.reduce_min(DATA[0],(1,2)))
        param.append(tf.reduce_min(DATA[1],(1,2)))
        param.append(tf.reduce_min(DATA[2],(1,2)))
        param.append(tf.reduce_min(DATA[3],(1,2)))
        param.append(tf.reduce_min(DATA[4],(1,2)))        
       else:
        if TYPE=='max':
         param.append(tf.reduce_max(DATA[0],(1,2)))
         param.append(tf.reduce_max(DATA[1],(1,2)))
         param.append(tf.reduce_max(DATA[2],(1,2)))
         param.append(tf.reduce_max(DATA[3],(1,2)))
         param.append(tf.reduce_max(DATA[4],(1,2)))         
 return param

def map_scale(FRAME,SCALES): 
 def scale(X):
  x=tf.divide(X[0],X[1])
  return x,X[1]
 shape  =FRAME.get_shape().as_list()
 _frame =[]
 for i in range(shape[3]):
  temp,_=tf.map_fn(scale,(FRAME[:,:,:,i],SCALES[i]))
  _frame.append(temp)
 frame  =tf.stack(_frame,axis=3)  
 return frame

def map_std(FRAME,MOMENTS):
 def std(X):
  x=tf.divide(tf.subtract(X[0],X[1][0]),tf.sqrt(X[1][1]))
  return x,(X[1][0],X[1][1])
 shape  =FRAME.get_shape().as_list()
 _frame =[]
 for i in range(shape[3]):
  temp,_=tf.map_fn(std,(FRAME[:,:,:,i],(MOMENTS[i][0],MOMENTS[i][1])))
  _frame.append(temp)
 frame  =tf.stack(_frame,axis=3)  
 return frame

def map_norm(FRAME,MINMAX):
 def norm(X):
  x=tf.divide(tf.subtract(X[0],X[1][0]),tf.subtract(X[1][1],X[1][0]))
  return x,(X[1][0],X[1][1])
 shape  =FRAME.get_shape().as_list()
 _frame =[]
 for i in range(shape[3]):
  temp,_=tf.map_fn(norm,(FRAME[:,:,:,i],(MINMAX[0][i],MINMAX[1][i])))       
  _frame.append(temp)
 frame  =tf.stack(_frame,axis=3)
 return frame

def NaN_Inf_FLAGS(DATA,FILENAME,nameflag=False):  
 NAN_FLAG=np.any(np.isnan(DATA))
 INF_FLAG=np.any(np.isinf(DATA))
 if nameflag:
  index=6
 else:
  index=7 
 if NAN_FLAG:
  print(' GENERATED VIDEO FOR ' +FILENAME.split('/')[index].split('.')[0]+' CONTAINS NaNs !')
 else:
  if INF_FLAG:
   print(' GENERATED VIDEO FOR '+FILENAME.split('/')[index].split('.')[0]+' CONTAINS Infs !')
  else:
   print(' GENERATED VIDEO FOR '+FILENAME.split('/')[index].split('.')[0]+' IS OK !'        )


