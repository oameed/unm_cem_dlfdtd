#################################
### UNIVERSITY OF NEW MEXICO  ###
### COMPUTATIONAL EM LAB      ###
### DEEP LEARNING PROJECT     ###
### NEURAL NETWORK DEFINITION ###
### by: OAMEED NOAKOASTEEN    ###
#################################

import os
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','..','lib'))
import numpy      as np
import tensorflow as tf
from paramd  import INTSCALE
from utilsd  import write_summaries_image,getPARAMS,map_scale,map_std,map_norm
from layersd import res_block,conv_linear,convolve,conv_trans_layer,rnn

#########################################
### FRAME SCALE/STANDARDIZE/NORMALIZE ###
#########################################
def scale_std_norm(FRAME,MODE): 
 with tf.name_scope('SCALE_STD_NORM'+'_'+MODE):
  FRAME=map_scale(FRAME, getPARAMS(FRAME,'scale'  ))  
  FRAME=map_std  (FRAME, getPARAMS(FRAME,'moments'))
  FRAME=map_norm (FRAME,[getPARAMS(FRAME,'min'    ) ,getPARAMS(FRAME,'max')])
 return FRAME

################################################
### OPERATIONS: RNN                          ###
### OPERATIONS: TRANSPOSED CONVOLUTION BLOCK ###
################################################
def rnn_op(X,B,MODE,VID_SHAPE,NAME='RECURRENT_TIME_FORWARDING'):
 with tf.name_scope(NAME+'_'+MODE):  
  with tf.name_scope('INIT_APPLY_BOUNDARY'):                                             # DO INITIAL MIXING OF IMAGE AND BOUNDARY   
   bound_sum,bound_mul=tf.split(B,2,axis=-1)                                             # SPLIT BOUNDARY INTO MULT AND SUM PARTS   
   rnn_cell           =rnn(bound_sum,bound_mul,MODE)                                     # DEFINE VANILLA RNN CELL USING MULT AND SUM BOUNDARY   
   X                  =tf.add(tf.multiply(X,bound_mul),bound_sum)                        # INITIAL MIXING OF IMAGE AND BOUNDARY  
  with tf.name_scope('ADD_DIMENSION_FOR_SCAN'):                                          # ADD THE FIRST DIMENSION FOR THE 'SCAN LOOP'
   x                  =tf.expand_dims(X,axis=0)
                                                                                         # INITIAL PREVIOUS HIDDEN/CELL STATE
                                                                                         # 'inith' REPRESENTS THE PREVIOUS HIDDEN STATE
                                                                                         # 'initc' REPRESENTS THE PREVIOUS CELL   STATE
  with tf.name_scope('INITIAL_PREV_OUT'):
   shape              =X.get_shape().as_list()
   inith              =tf.zeros(shape)
   initc              =tf.zeros(shape)  
  with tf.name_scope('DATA_STRUCT_FOR_SCAN'):                                            # GENERATE THE APPROPRIATE TENSOR FOR THE 'SCAN LOOP'
   zeroarray          =tf.cast(tf.constant(0,shape=(VID_SHAPE-1,shape[0],shape[1],shape[2],shape[3])),tf.float32)
   x                  =tf.concat((x,zeroarray),axis=0)
                                                                                         # RUN THE 'SCAN LOOP'
                                                                                         # LSTM: CURR[0]: CURRENT CELL   STATE
                                                                                         # LSTM: CURR[1]: CURRENT HIDDEN STATE
  x                   =tf.scan(rnn_cell,x,initializer=[initc,inith],name='GENERATE_FUTURE_FRAMES')[1]
 return x

def conv_transpose_make(MODE,CN):
 def conv_transpose_block(X):
  with tf.name_scope ('DECODER'):
   x=conv_trans_layer(X,64,32,[16 ,16 ],[3,3] ,'CONV_TRANS_1',MODE)
   x=res_block       (x,32,32                 ,'RES_BLOCK_21',MODE)
   x=res_block       (x,32,32                 ,'RES_BLOCK_22',MODE)
   x=conv_trans_layer(x,32,16,[32 ,32 ],[3,3] ,'CONV_TRANS_2',MODE)
   x=res_block       (x,16,16                 ,'RES_BLOCK_23',MODE)
   x=res_block       (x,16,16                 ,'RES_BLOCK_24',MODE)
   x=conv_trans_layer(x,16,8 ,[64 ,64 ],[3,3] ,'CONV_TRANS_3',MODE)
   x=res_block       (x,8 ,8                  ,'RES_BLOCK_25',MODE)
   x=res_block       (x,8 ,8                  ,'RES_BLOCK_26',MODE)
   x=conv_trans_layer(x,8 ,CN,[128,128],[3,3] ,'CONV_TRANS_4',MODE)
   x=res_block       (x,CN,CN                 ,'RES_BLOCK_27',MODE)
   x=res_block       (x,CN,CN                 ,'RES_BLOCK_28',MODE)   
   x=conv_linear     (x,CN,CN                 ,'LINEAR_1'    ,MODE)                      # APPLY A LINEAR LAYER
  return x
 return conv_transpose_block

##########################
### NETWORK DEFINITION ###
##########################
def NET(X,B,MODE,VIDEOMODE=False):
 #!!! ATTENTION: DECODER LAYER DEFINITIONS ARE IN 'conv_transpose_block' FUNCTION       !!!#
 #!!! ATTENTION: 'VIDEOMODE' ENABLES CONTINUOUS VIDEO GENERATION AFTER TRAINING/TESTING !!!#
 DATASHAPE         =X.get_shape().as_list()
 VID_SHAPE         =DATASHAPE[1] 
 if np.logical_not(VIDEOMODE):                                                           # PRE-PROCESSING  
  index            =0                                                                    # IN TRAIN/TEST MODE CHOOSE FIRST TIME FRAME
 else:  
  index            =VID_SHAPE-1                                                          # IN CONTINUOUS VIDEO GENERATION MODE CHOOSE LAST  TIME FRAME
 with tf.name_scope('GET_FIRST_TIME_STEP'+'_'+MODE):                                     # GET FIRST (OR LAST) TIME FRAME
  x                =tf.transpose(X,perm=[1,0,2,3,4])[index]
  bound            =tf.transpose(B,perm=[1,0,2,3,4])[index] 
 if not INTSCALE:
  x                 =scale_std_norm(x,MODE)                                              # SCALE/STANDARDIZE/NORMALIZE THE SELECTED FRAME  
 with tf.name_scope('ENCODER'+'_'+MODE):                                                 # ENCODER
  x                =res_block          (x    ,DATASHAPE[4] ,  8, 'RES_BLOCK_1' , MODE)
  x                =res_block          (x    ,  8          ,  8, 'RES_BLOCK_2' , MODE)
  x                =res_block          (x    ,  8          ,  8, 'RES_BLOCK_3' , MODE)
  x                =res_block          (x    ,  8          , 16, 'RES_BLOCK_4' , MODE)
  x                =res_block          (x    , 16          , 16, 'RES_BLOCK_5' , MODE)
  x                =res_block          (x    , 16          , 16, 'RES_BLOCK_6' , MODE)
  x                =res_block          (x    , 16          , 32, 'RES_BLOCK_7' , MODE)
  x                =res_block          (x    , 32          , 32, 'RES_BLOCK_8' , MODE)
  x                =res_block          (x    , 32          , 32, 'RES_BLOCK_9' , MODE)
  x                =res_block          (x    , 32          , 64, 'RES_BLOCK_10', MODE)
  x                =res_block          (x    , 64          , 64, 'RES_BLOCK_11', MODE)
  x                =res_block          (x    , 64          , 64, 'RES_BLOCK_12', MODE)
  bound            =res_block          (bound,  1          , 16, 'RES_BLOCK_13', MODE)
  bound            =res_block          (bound, 16          , 16, 'RES_BLOCK_14', MODE)
  bound            =res_block          (bound, 16          , 32, 'RES_BLOCK_15', MODE)
  bound            =res_block          (bound, 32          , 32, 'RES_BLOCK_16', MODE)
  bound            =res_block          (bound, 32          , 64, 'RES_BLOCK_17', MODE)
  bound            =res_block          (bound, 64          , 64, 'RES_BLOCK_18', MODE)
  bound            =res_block          (bound, 64          ,128, 'RES_BLOCK_19', MODE)
  bound            =res_block          (bound,128          ,128, 'RES_BLOCK_20', MODE) 
 x                 =rnn_op             (x,bound,MODE,VID_SHAPE                       )   # RNN 
 conv_transpose_op =conv_transpose_make(MODE,DATASHAPE[4]                            )   # DECODER
 x                 =tf.map_fn          (conv_transpose_op,x,name='DECODER'+'_'+MODE  )
                                                                                         # POST-PROCESSING
                                                                                         # RESHAPE GENERATED TIME STEPS TO VIDEO FORMAT
 with tf.name_scope('MAKE_VIDEO_EXAMPLE'+'_'+MODE):
  x                =tf.transpose(x,perm=[1,0,2,3,4]) 
 if np.logical_not(VIDEOMODE):                                                           # SUMMARY OPS
  tf.compat.v1.add_to_collection(tf.compat.v1.GraphKeys.ACTIVATIONS,X)
  tf.compat.v1.add_to_collection(tf.compat.v1.GraphKeys.ACTIVATIONS,B)
  tf.compat.v1.add_to_collection(tf.compat.v1.GraphKeys.ACTIVATIONS,x)  
 return x


